import { partialFactory, PartialFactory, SequenceGenerator } from '@pe/cucumber-sdk';
import * as uuid from 'uuid';
import { ApplicationCompiledThemeModel } from '../../../../src/modules/templating';

const seq: SequenceGenerator = new SequenceGenerator();

const defaultFactory: any = (): any => {
  seq.next();

  return ({
    _id: uuid.v4(),
    actions: [defaultActionsFactory()],
  });
};

const defaultActionsFactory: any = (): any => {
  seq.next();

  return ({
    id: uuid.v4(),
    effects: [
      {
        type: 'template:init',
        target: uuid.v4(),
        payload: {
          id: uuid.v4(),
          children: [],
        }
      },
      {
        type: 'stylesheet:init',
        target: uuid.v4(),
        payload: {}
      },
    ]
  });
};

export class ThemeSourcesFactory {
  public static create: PartialFactory<ThemeSourcesFactory> = partialFactory<ThemeSourcesFactory>(defaultFactory);

  public static createAction = partialFactory<any>(defaultActionsFactory);
}
