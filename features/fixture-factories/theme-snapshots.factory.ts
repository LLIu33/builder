import { partialFactory, PartialFactory, SequenceGenerator } from '@pe/cucumber-sdk';
import * as uuid from 'uuid';
import { ApplicationCompiledThemeModel } from '../../../../src/modules/templating';
import { ThemeSnapshotModel } from '../../src/themes/interfaces/entities';
import { PebPageType } from '@pe/builder-core';

const seq: SequenceGenerator = new SequenceGenerator();

const DEFAULT_FRONT_PAGE_ID: string = '11111111-1111-1111-1111-111111111111';

const defaultFactory: any = (): any => {
  seq.next();

  const pageTemplateId: string = uuid.v4();
  const desktopStylesheetId: string = uuid.v4();
  const tabletStylesheetId: string = uuid.v4();
  const mobileStylesheetId: string = uuid.v4();
  const pageContextId: string = uuid.v4();

  const pages: any = {
    [DEFAULT_FRONT_PAGE_ID]: {
      id: DEFAULT_FRONT_PAGE_ID,
      variant: 'front',
      type: PebPageType.Replica,
      templateId: pageTemplateId,
      stylesheetIds: {
        desktop: desktopStylesheetId,
        tablet: tabletStylesheetId,
        mobile: mobileStylesheetId,
      },
      contextId: pageContextId,
    },
  };

  return ({
    _id: uuid.v4(),
    pages,
    templates: {
      [pageTemplateId]: {},
    },
    stylesheets: {
      [desktopStylesheetId]: {},
      [tabletStylesheetId]: {},
      [mobileStylesheetId]: {},
    },
    contextSchemas: {
      [pageContextId]: {},
    },
    shop: {
      data: {},
      routing: [],
      contextId: pageContextId,
    }
  });
};

export class ThemeSnapshotsFactory {
  public static create: PartialFactory<ThemeSnapshotModel> = partialFactory<ThemeSnapshotModel>(defaultFactory);
}


