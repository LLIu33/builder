import { partialFactory, PartialFactory, SequenceGenerator } from '@pe/cucumber-sdk';
import * as uuid from 'uuid';
import { ThemeModel } from '../../src/themes/interfaces/entities';
import { ThemeType } from '../../src/themes/enums';

const seq: SequenceGenerator = new SequenceGenerator();

const defaultFactory: any = (): any => {
  seq.next();

  return ({
    _id: uuid.v4(),
    name: `Theme ${seq.current}`,
    source: uuid.v4(),
    isDefault: false,
    picture: `Image ${seq.current}`,
    publishedVersion: uuid.v4(),
    source: uuid.v4(),
    type: ThemeType.Template,
    versions: [uuid.v4()],
  });
};

export class ThemesFactory {
  public static create: PartialFactory<ThemeModel>
    = partialFactory<ThemeModel>(defaultFactory);
}
