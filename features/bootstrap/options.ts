import {
  AuthContext,
  AxiosContext,
  CucumberOptionsInterface,
  DatabaseContext,
  HttpContext,
  InMemoryProvider,
  LoggerContext,
  RabbitMqContext,
  RedisContext,
  StorageContext,
  WorldContext,
} from '@pe/cucumber-sdk/module/';
import { HttpProvider } from '@pe/cucumber-sdk/module/http';
import { RabbitMqProvider } from '@pe/cucumber-sdk/module/rabbit';
import { RedisProvider } from '@pe/cucumber-sdk/module/redis';
import * as dotenv from 'dotenv';
import * as path from 'path';
import { FastifyAdapter } from '@nestjs/platform-fastify';

import { AppConfigurator } from './app.configurator';

dotenv.config({
  path: path.resolve('./features/app/.env'),
});
const env: NodeJS.ProcessEnv = process.env;

export const options: CucumberOptionsInterface = {
  appConfigurator: AppConfigurator,
  contexts: [
    LoggerContext,
    AuthContext,
    DatabaseContext,
    WorldContext,
    HttpContext,
    AxiosContext,
    StorageContext,
    RabbitMqContext,
    RedisContext,
  ],
  dataPath: path.resolve('./features/data'),
  fixtures: path.resolve('./features/fixtures'),
  httpAdapter: { class: FastifyAdapter, options: {} },
  mongodb: env.MONGODB_URL,
  providers: [
    HttpProvider,
    InMemoryProvider,
    RabbitMqProvider,
    RedisProvider,
  ],
};
