import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { RabbitMqModule, RedisModule } from '@pe/nest-kit';
import { JwtAuthModule } from '@pe/nest-kit/modules/auth';
import { NestKitLoggingModule } from '@pe/nest-kit/modules/logging';
import { environment } from './environments';
import { BuilderModule } from '../../src';
import { BusinessSchema, BusinessSchemaName, TestApplicationSchema, TestApplicationSchemaName } from './schemas';
import { ApplicationTypesEnum } from '../../src/common/enums';

@Module({
  controllers: [  ],
  imports: [
    NestKitLoggingModule.forRoot({
      applicationName: environment.applicationName,
      isProduction: environment.production,
    }),
    JwtAuthModule.forRoot(environment.jwtOptions),
    RedisModule.forRoot(environment.redis),
    MongooseModule.forRoot(
      environment.mongodb,
      {
        useCreateIndex: true,
        useFindAndModify: false,
        useNewUrlParser: true,
        useUnifiedTopology: true,
      },
    ),
    MongooseModule.forFeature(
      [
        {
          name: TestApplicationSchemaName,
          schema: TestApplicationSchema,
        },
        {
          name: BusinessSchemaName,
          schema: BusinessSchema,
        },
      ],
    ),
    RabbitMqModule.forRoot(environment.rabbitmq),
    BuilderModule.forRoot({
      applicationSchemaName: TestApplicationSchemaName,
      applicationSchema: TestApplicationSchema,
      applicationType: ApplicationTypesEnum.Shop,
      compilationQueuePrefix: environment.rabbitmq.compilationQueuePrefix,
      jwtSecret: environment.jwtOptions.secret,
    }),
  ],
})

export class AppModule implements NestModule {
  public configure(): MiddlewareConsumer | void {}
}
