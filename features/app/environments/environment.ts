/* tslint:disable:object-literal-sort-keys */
import * as dotenv from 'dotenv';
import * as path from 'path';

dotenv.config({
  path: path.resolve('./features/app/.env'),
});
const env: NodeJS.ProcessEnv = process.env;
const isNumeric: (n: any) => boolean =
  (n: any): boolean => !isNaN(parseInt(n, 10)) && isFinite(n)
;

export const environment: any = {
  production: env.PRODUCTION_MODE === 'true',
  applicationName: env.APP_NAME,
  port: env.APP_PORT,
  mongodb: env.MONGODB_URL,
  refreshTokenExpiresIn: isNumeric(env.JWT_REFRESH_TOKEN_EXPIRES_IN)
    ? parseInt(env.JWT_REFRESH_TOKEN_EXPIRES_IN, 10)
    : env.JWT_REFRESH_TOKEN_EXPIRES_IN,
  jwtOptions: {
    secret: env.JWT_SECRET_TOKEN,
    jwtKeyExtractorOptions: {
      tokenQueryParameterName: 'token',
    },
    signOptions: {
      expiresIn: isNumeric(env.JWT_EXPIRES_IN)
        ? parseInt(env.JWT_EXPIRES_IN, 10)
        : env.JWT_EXPIRES_IN
      ,
    },
  },
  rabbitmq: {
    compilationQueuePrefix: env.QUEUE_PREFIX,
    urls: [env.RABBITMQ_URL],
    queues: [],
    exchanges: [
      {
        name: 'async_events',
        options: { durable: true },
        queues: [
          {
            name: 'async_events_test_micro',
            options: {
              deadLetterExchange: 'async_events_fallback',
              deadLetterRoutingKey: 'async_events_test_micro',
              durable: true,
            },
          },
        ],
        type: 'direct',
      },
    ],
    prefetchCount: 1,
    isGlobalPrefetchCount: false,
  },
  redis: { },

  rsa: {
    private: path.resolve(env.RABBITMQ_CERTIFICATE_PATH),
  },
  webSocket: {
    port: env.WS_PORT,
  },
};
