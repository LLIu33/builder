import { Schema } from 'mongoose';
import { v4 as uuid } from 'uuid';
import { BusinessSchemaName } from './business.schema';

export const TestApplicationSchemaName: string = 'TestApplication';
export const TestApplicationSchema: Schema = new Schema(
  {
    _id: { type: String, default: uuid },
    business: { type: String, ref: BusinessSchemaName },
    name: String,
  },
  {
    timestamps: true,
  },
);
