import { getModelToken } from '@nestjs/mongoose';
import { BaseFixture } from '@pe/cucumber-sdk';
import { Model } from 'mongoose';
import { BusinessSchemaName, TestApplicationSchemaName } from '../../app/schemas';
import { ThemesSchemaNamesEnum } from '../../../src/themes';
import { ThemeSnapshotModel, ThemeSourceModel } from '../../../src/themes/interfaces/entities';
import { ThemesFactory, ThemeSnapshotsFactory, ThemeSourcesFactory } from '../../fixture-factories';
import { TemplateThemeItemModel } from '../../../src/template-theme/interfaces/entities';
import { TemplateSchemaNamesEnum } from '../../../src/template-theme/enums';

const APPLICATION_ID: string = 'cccccccc-cccc-cccc-cccc-cccccccccccc';
const BUSINESS_ID: string = 'bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb';
const THEME_ID: string = 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa';
const TEMPLATE_GROUP_ID: string = 'dddddddd-dddd-dddd-dddd-dddddddddddd';
const TEMPLATE_ITEM_ID: string = 'ffffffff-ffff-ffff-ffff-ffffffffffff';

class MarkThemeAsTemplateGroupExistsFixture extends BaseFixture {
  private appModel: Model<any> = this.application.get(getModelToken(TestApplicationSchemaName));
  private businessModel: Model<any> = this.application.get(getModelToken(BusinessSchemaName));


  private themeModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.Theme));
  private themeSourceModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.ThemeSource));
  private themeSnapshotModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.ThemeSnapshot));

  private templateThemeGroup: Model<any> = this.application
    .get(getModelToken(TemplateSchemaNamesEnum.TemplateThemeGroup));
  private templateThemeItem: Model<any> = this.application
    .get(getModelToken(TemplateSchemaNamesEnum.TemplateThemeItem));

  public async apply(): Promise<void> {
    await this.appModel.create({
      _id: APPLICATION_ID,
      name: 'Test app',
      business: BUSINESS_ID,
    });

    await this.businessModel.create({
      _id: BUSINESS_ID,
      name: 'Test business',
    });

    await this.createDefaultTemplate(THEME_ID);

    const item:TemplateThemeItemModel = await this.templateThemeItem.create({
      _id: TEMPLATE_ITEM_ID,
      code: 'testItem',
      themes: [THEME_ID],
    });

    await this.templateThemeGroup.create({
      _id: TEMPLATE_GROUP_ID,
      code: 'testGroup',
      items: [item._id],
    });
  }

  private async createDefaultTemplate(themeId: string): Promise<void> {
    const snapshot: ThemeSnapshotModel = await this.themeSnapshotModel.create(ThemeSnapshotsFactory.create({ }));

    const source: ThemeSourceModel = await this.themeSourceModel.create(
      ThemeSourcesFactory.create({
        snapshot,
      }),
    );

    await this.themeModel.create(
      ThemesFactory.create({
        _id: themeId,
        name: 'Test theme',
        source: source._id,
      }),
    );
  }
}

export = MarkThemeAsTemplateGroupExistsFixture;
