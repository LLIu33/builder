import { getModelToken } from '@nestjs/mongoose';
import { BaseFixture } from '@pe/cucumber-sdk';
import { Model } from 'mongoose';
import { ThemesSchemaNamesEnum } from '../../../src/themes';
import { ThemeSnapshotModel, ThemeSourceModel } from '../../../src/themes/interfaces/entities';

const THEME_ID: string = 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa';
const THEME_SOURCE_ID: string = 'cccccccc-cccc-cccc-cccc-cccccccccccc';
const THEME_SNAPSHOT_ID: string = 'ffffffff-ffff-ffff-ffff-ffffffffffff';
const PAGE_ID: string = '11111111-1111-1111-1111-111111111111';
const FORK_PAGE_ID: string = '22222222-2222-2222-2222-222222222222';
const ANOTHER_FORK_PAGE_ID: string = '33333333-3333-3333-3333-3333333333';
const NON_FORK_PAGE_ID: string = '44444444-4444-4444-4444-4444444444';
const FIRST_ACTION: string = 'first-action';
const SECOND_ACTION: string = 'second-action';

class ThemeExistsFixture extends BaseFixture {

  private themeModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.Theme));
  private themeSourceModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.ThemeSource));
  private themeSnapshotModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.ThemeSnapshot));

  public async apply(): Promise<void> {
    const snapshot: ThemeSnapshotModel = await this.themeSnapshotModel.create({
      _id: THEME_SNAPSHOT_ID,
      pages: {
        [PAGE_ID]: {
          id: PAGE_ID,
          variant: 'front',
          type: 'master',
          master: null,
          templateId: '0efed217-c12c-4988-8d3a-92ec5f267d97',
          stylesheetIds: {
            desktop: '0e0203d4-c6d1-48cd-98ec-4cf7d1be9dc2',
            tablet: '07bcb55b-f788-4380-9ab4-5db71c1d3022',
            mobile: '05ce8ca0-9f04-4019-abc6-d85349a122b3',
          },
          contextId: '00b8194d-d72b-4427-86f4-920552cea9ea',
        },
        [FORK_PAGE_ID]: {
          id: FORK_PAGE_ID,
          variant: 'front',
          type: 'replica',
          master: {
            id: PAGE_ID,
            lastActionId: FIRST_ACTION,
          },
          templateId: '1efed217-c12c-4988-8d3a-92ec5f267d97',
          stylesheetIds: {
            desktop: '1e0203d4-c6d1-48cd-98ec-4cf7d1be9dc2',
            tablet: '17bcb55b-f788-4380-9ab4-5db71c1d3022',
            mobile: '15ce8ca0-9f04-4019-abc6-d85349a122b3',
          },
          contextId: '10b8194d-d72b-4427-86f4-920552cea9ea',
        },
        [ANOTHER_FORK_PAGE_ID]: {
          id: ANOTHER_FORK_PAGE_ID,
          variant: 'front',
          type: 'replica',
          master: {
            id: PAGE_ID,
            lastActionId: SECOND_ACTION,
          },
          templateId: '2efed217-c12c-4988-8d3a-92ec5f267d97',
          stylesheetIds: {
            desktop: '2e0203d4-c6d1-48cd-98ec-4cf7d1be9dc2',
            tablet: '27bcb55b-f788-4380-9ab4-5db71c1d3022',
            mobile: '25ce8ca0-9f04-4019-abc6-d85349a122b3',
          },
          contextId: '20b8194d-d72b-4427-86f4-920552cea9ea',
        },
        [NON_FORK_PAGE_ID]: {
          id: NON_FORK_PAGE_ID,
          variant: 'front',
          type: 'replica',
          master: null,
          templateId: 'befed217-c12c-4988-8d3a-92ec5f267d97',
          stylesheetIds: {
            desktop: '4e0203d4-c6d1-48cd-98ec-4cf7d1be9dc2',
            tablet: 'd7bcb55b-f788-4380-9ab4-5db71c1d3022',
            mobile: '55ce8ca0-9f04-4019-abc6-d85349a122b3',
          },
          contextId: '00b8194d-d72b-4427-86f4-920552cea9ea',
        },
      },
    });

    const source: ThemeSourceModel = await this.themeSourceModel.create({
      _id: THEME_SOURCE_ID,
      actions: [
        {
          id: FIRST_ACTION,
          targetPageId: PAGE_ID,
          affectedPageIds: [PAGE_ID],
          createdAt: '2020-08-18T11:12:11.377Z',
          effects: [
            {
              type: 'page:init',
              target: `pages:${PAGE_ID}`,
              payload: {
                id: PAGE_ID,
                variant: 'front',
                type: 'master',
                master: null,
                templateId: '0efed217-c12c-4988-8d3a-92ec5f267d97',
                stylesheetIds: {
                  desktop: '0e0203d4-c6d1-48cd-98ec-4cf7d1be9dc2',
                  tablet: '07bcb55b-f788-4380-9ab4-5db71c1d3022',
                  mobile: '05ce8ca0-9f04-4019-abc6-d85349a122b3',
                },
                contextId: '00b8194d-d72b-4427-86f4-920552cea9ea',
              },
            },
          ],
        },
        {
          id: SECOND_ACTION,
          targetPageId: PAGE_ID,
          affectedPageIds: [PAGE_ID],
          createdAt: '2020-08-18T12:10:36.384Z',
          effects: [
            {
              type: 'template:init',
              target: 'templates:0efed217-c12c-4988-8d3a-92ec5f267d97',
              payload: {
                id: '111',
                type: 'document',
              },
            },
            {
              type: 'stylesheet:init',
              target: 'stylesheets:0e0203d4-c6d1-48cd-98ec-4cf7d1be9dc2',
              payload: { },
            },
            {
              type: 'context-schema:init',
              target: 'contextSchemas:00b8194d-d72b-4427-86f4-920552cea9ea',
              payload: { },
            },
          ],
        },
        {
          id: 'third-action',
          targetPageId: FORK_PAGE_ID,
          affectedPageIds: [FORK_PAGE_ID],
          createdAt: '2020-08-18T11:12:11.377Z',
          effects: [
            {
              type: 'page:init',
              target: `pages:${FORK_PAGE_ID}`,
              payload: {
                id: FORK_PAGE_ID,
                variant: 'front',
                type: 'replica',
                master: {
                  id: PAGE_ID,
                  lastActionId: FIRST_ACTION,
                  idsMap: {},
                },
                templateId: '1efed217-c12c-4988-8d3a-92ec5f267d97',
                stylesheetIds: {
                  desktop: '1e0203d4-c6d1-48cd-98ec-4cf7d1be9dc2',
                  tablet: '17bcb55b-f788-4380-9ab4-5db71c1d3022',
                  mobile: '15ce8ca0-9f04-4019-abc6-d85349a122b3',
                },
                contextId: '10b8194d-d72b-4427-86f4-920552cea9ea',
              },
            },
          ],
        },
      ],
      snapshot,
    });

    await this.themeModel.create({
      _id: THEME_ID,
      name: 'Test theme',
      source,
    });
  }
}

export = ThemeExistsFixture;
