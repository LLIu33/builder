import { getModelToken } from '@nestjs/mongoose';
import { BaseFixture } from '@pe/cucumber-sdk';
import { Model } from 'mongoose';
import { ThemesSchemaNamesEnum } from '../../../src/themes';
import { ThemeSnapshotModel, ThemeSourceModel } from '../../../src/themes/interfaces/entities';

const THEME_ID: string = 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa';
const THEME_SOURCE_ID: string = 'cccccccc-cccc-cccc-cccc-cccccccccccc';
const THEME_SNAPSHOT_ID: string = 'ffffffff-ffff-ffff-ffff-ffffffffffff';
const PAGE_ID: string = '11111111-1111-1111-1111-111111111111';
const SNAPSHOT_HASH: string = '4baf9221';

class ThemeExistsFixture extends BaseFixture {

  private themeModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.Theme));
  private themeSourceModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.ThemeSource));
  private themeSnapshotModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.ThemeSnapshot));

  public async apply(): Promise<void> {
    const snapshot: ThemeSnapshotModel = await this.themeSnapshotModel.create({
      '_id': THEME_SNAPSHOT_ID,
      'contextSchemas': {
        '7072c359-e8b2-407f-85cb-56c9530c0d0d': {},
        'cb8d238d-be50-48c7-9e85-33d5160ba82b': {},
      },
      'hash': SNAPSHOT_HASH,
      'pages': {
        [PAGE_ID]: {
          'id': PAGE_ID,
          'type': 'master',
          'variant': 'front',
          'master': null,
          'lastActioId': 'test-action',
          'name': 'Front',
          'data': {
            'mark': null,
          },
          'templateId': 'cedcd983-ed50-4eb6-81c8-3d3a5cdc43c9',
          'stylesheetIds': {
            'desktop': '2e0af977-3ab4-4441-bf82-1e1a1adcd39e',
            'tablet': 'd33f0564-5e8d-4257-a74b-8bbfc6c6ba18',
            'mobile': '25d0ebb6-7c76-4131-9078-ce3c655d231d',
          },
          'contextId': '7072c359-e8b2-407f-85cb-56c9530c0d0d',
        },
      },
      'shop': {
        'data': {
          'productPages': '/products/:productId',
          'categoryPages': '/categories/:categoryId',
        },
        'routing': [
          {
            'routeId': '47a02513-371b-428e-a87d-202c5af631f9',
            'url': '/',
            'pageId': PAGE_ID,
          },
        ],
        'contextId': 'cb8d238d-be50-48c7-9e85-33d5160ba82b',
        'pageIds': [
          PAGE_ID,
        ],
      },
      'stylesheets': {
        '2e0af977-3ab4-4441-bf82-1e1a1adcd39e': {
          '63dee3f3-e20e-495e-871d-405ad1619c05': {},
          '12e89f53-0165-4d9f-b0be-726ba848bbd6': {
            'height': 200,
          },
          'da7bee8b-d71f-44b8-8dea-5c434517d4be': {
            'height': 600,
          },
          '461d081c-0a0c-4fe5-baaa-197ff4cea2e9': {
            'height': 200,
          },
        },
        'd33f0564-5e8d-4257-a74b-8bbfc6c6ba18': {
          '63dee3f3-e20e-495e-871d-405ad1619c05': {},
          '12e89f53-0165-4d9f-b0be-726ba848bbd6': {
            'height': 200,
          },
          'da7bee8b-d71f-44b8-8dea-5c434517d4be': {
            'height': 600,
          },
          '461d081c-0a0c-4fe5-baaa-197ff4cea2e9': {
            'height': 200,
          },
        },
        '25d0ebb6-7c76-4131-9078-ce3c655d231d': {
          '63dee3f3-e20e-495e-871d-405ad1619c05': {},
          '12e89f53-0165-4d9f-b0be-726ba848bbd6': {
            'height': 200,
          },
          'da7bee8b-d71f-44b8-8dea-5c434517d4be': {
            'height': 600,
          },
          '461d081c-0a0c-4fe5-baaa-197ff4cea2e9': {
            'height': 200,
          },
        },
      },
      'templates': {
        'cedcd983-ed50-4eb6-81c8-3d3a5cdc43c9': {
          'id': '63dee3f3-e20e-495e-871d-405ad1619c05',
          'type': 'document',
          'children': [
            {
              'id': '12e89f53-0165-4d9f-b0be-726ba848bbd6',
              'type': 'section',
              'data': {
                'name': 'header',
              },
              'meta': {
                'deletable': false,
              },
              'children': [],
            },
            {
              'id': 'da7bee8b-d71f-44b8-8dea-5c434517d4be',
              'type': 'section',
              'data': {
                'name': 'body',
              },
              'meta': {
                'deletable': false,
              },
              'children': [],
            },
            {
              'id': '461d081c-0a0c-4fe5-baaa-197ff4cea2e9',
              'type': 'section',
              'data': {
                'name': 'footer',
              },
              'meta': {
                'deletable': false,
              },
              'children': [],
            },
          ],
        },
      },
      'id': THEME_SNAPSHOT_ID,
    });

    const source: ThemeSourceModel = await this.themeSourceModel.create({
      '_id': THEME_SOURCE_ID,
      'actions': [
        {
          'id': 'first-action',
          'targetPageId': PAGE_ID,
          'affectedPageIds': [PAGE_ID],
          'effects': [{
            'type': 'stylesheet:init',
            'target': 'stylesheets:2e0af977-3ab4-4441-bf82-1e1a1adcd39e',
            'payload': {
              '63dee3f3-e20e-495e-871d-405ad1619c05': {},
              '12e89f53-0165-4d9f-b0be-726ba848bbd6': {
                'height': 200,
              },
              'da7bee8b-d71f-44b8-8dea-5c434517d4be': {
                'height': 600,
              },
              '461d081c-0a0c-4fe5-baaa-197ff4cea2e9': {
                'height': 200,
              },
            },
          }],
        },
        {
          'id': 'test-action',
          'targetPageId': PAGE_ID,
          'affectedPageIds': [
            PAGE_ID,
          ],
          'effects': [
            {
              'type': 'template:init',
              'target': 'templates:cedcd983-ed50-4eb6-81c8-3d3a5cdc43c9',
              'payload': {
                'id': '63dee3f3-e20e-495e-871d-405ad1619c05',
                'type': 'document',
                'children': [
                  {
                    'id': '12e89f53-0165-4d9f-b0be-726ba848bbd6',
                    'type': 'section',
                    'data': {
                      'name': 'header',
                    },
                    'meta': {
                      'deletable': false,
                    },
                    'children': [],
                  },
                  {
                    'id': 'da7bee8b-d71f-44b8-8dea-5c434517d4be',
                    'type': 'section',
                    'data': {
                      'name': 'body',
                    },
                    'meta': {
                      'deletable': false,
                    },
                    'children': [],
                  },
                  {
                    'id': '461d081c-0a0c-4fe5-baaa-197ff4cea2e9',
                    'type': 'section',
                    'data': {
                      'name': 'footer',
                    },
                    'meta': {
                      'deletable': false,
                    },
                    'children': [],
                  },
                ],
              },
            },
            {
              'type': 'stylesheet:init',
              'target': 'stylesheets:2e0af977-3ab4-4441-bf82-1e1a1adcd39e',
              'payload': {
                '63dee3f3-e20e-495e-871d-405ad1619c05': {},
                '12e89f53-0165-4d9f-b0be-726ba848bbd6': {
                  'height': 200,
                },
                'da7bee8b-d71f-44b8-8dea-5c434517d4be': {
                  'height': 600,
                },
                '461d081c-0a0c-4fe5-baaa-197ff4cea2e9': {
                  'height': 200,
                },
              },
            },
            {
              'type': 'stylesheet:init',
              'target': 'stylesheets:d33f0564-5e8d-4257-a74b-8bbfc6c6ba18',
              'payload': {
                '63dee3f3-e20e-495e-871d-405ad1619c05': {},
                '12e89f53-0165-4d9f-b0be-726ba848bbd6': {
                  'height': 200,
                },
                'da7bee8b-d71f-44b8-8dea-5c434517d4be': {
                  'height': 600,
                },
                '461d081c-0a0c-4fe5-baaa-197ff4cea2e9': {
                  'height': 200,
                },
              },
            },
            {
              'type': 'stylesheet:init',
              'target': 'stylesheets:25d0ebb6-7c76-4131-9078-ce3c655d231d',
              'payload': {
                '63dee3f3-e20e-495e-871d-405ad1619c05': {},
                '12e89f53-0165-4d9f-b0be-726ba848bbd6': {
                  'height': 200,
                },
                'da7bee8b-d71f-44b8-8dea-5c434517d4be': {
                  'height': 600,
                },
                '461d081c-0a0c-4fe5-baaa-197ff4cea2e9': {
                  'height': 200,
                },
              },
            },
            {
              'type': 'context-schema:init',
              'target': 'contextSchemas:7072c359-e8b2-407f-85cb-56c9530c0d0d',
              'payload': {},
            },
            {
              'type': 'page:init',
              'target': 'pages:' + PAGE_ID,
              'payload': {
                'id': PAGE_ID,
                'type': 'master',
                'variant': 'front',
                'master': null,
                'lastActioId': 'test-action',
                'name': 'Front',
                'data': {
                  'mark': null,
                },
                'templateId': 'cedcd983-ed50-4eb6-81c8-3d3a5cdc43c9',
                'stylesheetIds': {
                  'desktop': '2e0af977-3ab4-4441-bf82-1e1a1adcd39e',
                  'tablet': 'd33f0564-5e8d-4257-a74b-8bbfc6c6ba18',
                  'mobile': '25d0ebb6-7c76-4131-9078-ce3c655d231d',
                },
                'contextId': '7072c359-e8b2-407f-85cb-56c9530c0d0d',
              },
            },
            {
              'type': 'context-schema:init',
              'target': 'contextSchemas:cb8d238d-be50-48c7-9e85-33d5160ba82b',
              'payload': {},
            },
            {
              'type': 'shop:init',
              'target': 'shop',
              'payload': {
                'data': {
                  'productPages': '/products/:productId',
                  'categoryPages': '/categories/:categoryId',
                },
                'routing': [
                  {
                    'routeId': '47a02513-371b-428e-a87d-202c5af631f9',
                    'url': '/',
                    'pageId': PAGE_ID,
                  },
                ],
                'contextId': 'cb8d238d-be50-48c7-9e85-33d5160ba82b',
                'pageIds': [
                  PAGE_ID,
                ],
              },
            },
          ],
        },
      ],
      snapshot,
    });

    await this.themeModel.create({
      _id: THEME_ID,
      name: 'Test theme',
      source,
    });
  }
}

export = ThemeExistsFixture;
