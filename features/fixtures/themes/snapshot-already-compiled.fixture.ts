import { getModelToken } from '@nestjs/mongoose';
import { BaseFixture } from '@pe/cucumber-sdk';
import { Model } from 'mongoose';
import { ThemesSchemaNamesEnum } from '../../../src/themes';
import { ThemeSourceModel } from '../../../src/themes/interfaces/entities';
import { ActionsStateHelper } from '../../../src/themes/helpers';

const THEME_ID: string = 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa';
const THEME_SOURCE_ID: string = 'cccccccc-cccc-cccc-cccc-cccccccccccc';
const THEME_SNAPSHOT_ID: string = 'ffffffff-ffff-ffff-ffff-ffffffffffff';
const PAGE_ID: string = '11111111-1111-1111-1111-111111111111';

class SnapshotAlreadyCompiledFixture extends BaseFixture {

  private themeModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.Theme));
  private themeSourceModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.ThemeSource));
  private themeSnapshotModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.ThemeSnapshot));

  public async apply(): Promise<void> {
    const source: ThemeSourceModel = await this.themeSourceModel.create({
      _id: THEME_SOURCE_ID,
      actions: [
        {
          id: 'test-action',
          effects: [
            {
              type: 'template:init',
              target: 'shop',
              payload: {
                id: '111',
                children: [],
              }
            },
            {
              type: 'stylesheet:init',
              target: 'stylesheets',
              payload: {}
            },
          ]
        },
      ],
      snapshot: THEME_SNAPSHOT_ID,
    });

    await this.themeSnapshotModel.create({
      _id: THEME_SNAPSHOT_ID,
      pages: {
        [PAGE_ID]: {
          id: PAGE_ID,
          variant: 'front',
          name: 'Main Page',
          templateId: 'befed217-c12c-4988-8d3a-92ec5f267d97',
          stylesheetIds: {
            desktop: '4e0203d4-c6d1-48cd-98ec-4cf7d1be9dc2',
            tablet: 'd7bcb55b-f788-4380-9ab4-5db71c1d3022',
            mobile: '55ce8ca0-9f04-4019-abc6-d85349a122b3',
          },
          contextId: '00b8194d-d72b-4427-86f4-920552cea9ea',
        },
      },
      hash: ActionsStateHelper.getStateHash(source.actions),
      updatedAt: new Date('2020-08-20 00:00:00'),
    });

    await this.themeModel.create({
      _id: THEME_ID,
      name: 'Test theme',
      source,
    });
  }
}

export = SnapshotAlreadyCompiledFixture;
