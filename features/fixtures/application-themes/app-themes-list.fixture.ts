import { getModelToken } from '@nestjs/mongoose';
import { BaseFixture } from '@pe/cucumber-sdk';
import { Model } from 'mongoose';
import { ApplicationThemeSchemaNamesEnum } from '../../../src/application-theme/enums';
import { ThemesSchemaNamesEnum } from '../../../src/themes';
import { ThemeSnapshotModel, ThemeSourceModel } from '../../../src/themes/interfaces/entities';
import { BusinessSchemaName, TestApplicationSchemaName } from '../../app/schemas';
import { ThemesFactory, ThemeSnapshotsFactory, ThemeSourcesFactory } from '../../fixture-factories';

const APPLICATION_ID: string = 'cccccccc-cccc-cccc-cccc-cccccccccccc';
const APPLICATION_ID_2: string = 'cccccccc-cccc-cccc-cccc-222222222222';
const BUSINESS_ID: string = 'bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb';
const ANOTHER_BUSINESS_ID: string = 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa';
const FRONTPAGE_ID: string = '11111111-1111-1111-1111-222222222222';
const ABOUTPAGE_ID:string =  '33333333-3333-3333-4444-555555555555';

class AppThemesListFixture extends BaseFixture {
  private appModel: Model<any> = this.application.get(getModelToken(TestApplicationSchemaName));
  private businessModel: Model<any> = this.application.get(getModelToken(BusinessSchemaName));

  private themeModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.Theme));
  private themeSourceModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.ThemeSource));
  private themeSnapshotModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.ThemeSnapshot));

  private themeCompiledThemeModel: Model<any> = this.application
    .get(getModelToken(ApplicationThemeSchemaNamesEnum.CompiledTheme));
  private appThemeModel: Model<any> = this.application
    .get(getModelToken(ApplicationThemeSchemaNamesEnum.ApplicationTheme));
    private appPageModel: Model<any> = this.application
    .get(getModelToken(ApplicationThemeSchemaNamesEnum.ApplicationPage));

  public async apply(): Promise<void> {
    await this.appModel.create({
      _id: APPLICATION_ID,
      name: 'Test app',
      business: BUSINESS_ID,
    });

    await this.appModel.create({
      _id: APPLICATION_ID_2,
      name: 'Test app',
      business: BUSINESS_ID,
    });

    await this.businessModel.create({
      _id: BUSINESS_ID,
      name: 'Test business',
    });

    await this.businessModel.create({
      _id: ANOTHER_BUSINESS_ID,
      name: 'Another business',
    });

    await this.createApplicationTheme('11111111-1111-1111-1111-111111111111', APPLICATION_ID);
    await this.createApplicationTheme('22222222-2222-2222-2222-222222222222', APPLICATION_ID);
    await this.createApplicationTheme('33333333-3333-3333-4444-444444444444', APPLICATION_ID);
    await this.createApplicationTheme('55555555-5555-5555-5555-555555555555', APPLICATION_ID, true);
    await this.createApplicationTheme('44444444-4444-4444-4444-444444444444', APPLICATION_ID_2);

    await this.appPageModel.create({
      '_id': FRONTPAGE_ID,
      'application': APPLICATION_ID,
      'context': {},
      'data': {},
      'master': null,
      'name': 'Front Page',
      'stylesheets': {},
      'template': {},
      'type': 'replica',
      'variant': 'front',
  });

    await this.appPageModel.create({
      '_id': ABOUTPAGE_ID,
      'application': APPLICATION_ID,
      'context': {},
      'data': {},
      'master': null,
      'name': 'About Page',
      'stylesheets': {},
      'template': {},
      'type': 'replica',
      'variant': 'about',
    });

    await this.themeCompiledThemeModel.create({
      '_id': '074166dd-be99-4a36-b92e-55daf4031c1f',
      'pages': [FRONTPAGE_ID, ABOUTPAGE_ID],
      'application': APPLICATION_ID,
      'context': {},
      'data': {
          'productPages': '/products/:productId',
          'categoryPages': '/category/:categoryId',
      },
      'routing': [{
          'routeId': 'nike-main-page-route',
          'url': '/',
          'pageId': FRONTPAGE_ID,
      }, {
          'routeId': 'nike-about-page-route',
          'url': '/about',
          'pageId': ABOUTPAGE_ID,
      }],
    });
  }

  private async createApplicationTheme(
    themeId: string,
    applicationId: string,
    isActive: boolean = false,
  ): Promise<void> {
    const snapshot: ThemeSnapshotModel = await this.themeSnapshotModel.create(ThemeSnapshotsFactory.create({ }));

    const source: ThemeSourceModel = await this.themeSourceModel.create(
      ThemeSourcesFactory.create({
        snapshot,
      }),
    );

    await this.themeModel.create(
      ThemesFactory.create({
        _id: themeId,
        name: 'Test theme',
        source: source._id,
      }),
    );

    await this.appThemeModel.create({
      theme: themeId,
      application: applicationId,
      isActive,
    });
  }
}

export = AppThemesListFixture;
