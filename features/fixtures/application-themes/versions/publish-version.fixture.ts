import { getModelToken } from '@nestjs/mongoose';
import { BaseFixture } from '@pe/cucumber-sdk';
import { Model } from 'mongoose';
import { BusinessSchemaName, TestApplicationSchemaName } from '../../../app/schemas';
import { ThemesSchemaNamesEnum, ThemeType } from '../../../../src/themes';
import { ThemesFactory, ThemeSnapshotsFactory, ThemeSourcesFactory } from '../../../fixture-factories';
import { ThemeModel, ThemeSnapshotModel, ThemeSourceModel } from '../../../../src/themes/interfaces/entities';
import { ApplicationThemeSchemaNamesEnum } from '../../../../src/application-theme/enums';

const BUSINESS_ID: string = 'bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb';
const THEME_ID: string = 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa';
const THEME_VERSION_ID: string = 'dddddddd-dddd-dddd-dddd-dddddddddddd';
const APPLICATION_ID: string = 'cccccccc-cccc-cccc-cccc-cccccccccccc';

class PublishVersionFixture extends BaseFixture {
  private appModel: Model<any> = this.application.get(getModelToken(TestApplicationSchemaName));
  private appThemeModel: Model<any> = this.application
    .get(getModelToken(ApplicationThemeSchemaNamesEnum.ApplicationTheme));
  private businessModel: Model<any> = this.application.get(getModelToken(BusinessSchemaName));
  private themeModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.Theme));
  private themeSourceModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.ThemeSource));
  private themeSnapshotModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.ThemeSnapshot));
  private themeVersionModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.ThemeVersion));

  public async apply(): Promise<void> {

    const appModel: any = await this.appModel.create({
      _id: APPLICATION_ID,
      name: 'Test app',
      business: BUSINESS_ID,
    });

    await this.businessModel.create({
      _id: BUSINESS_ID,
      name: 'Test business',
    });

    const theme: ThemeModel = await this.createTheme(THEME_ID);

    await this.appThemeModel.create({
      application: appModel,
      theme,
    })
  }

  private async createTheme(themeId: string): Promise<void> {
    const snapshot: ThemeSnapshotModel = await this.themeSnapshotModel.create(ThemeSnapshotsFactory.create({ }));

    const source: ThemeSourceModel = await this.themeSourceModel.create(
      ThemeSourcesFactory.create({
        snapshot,
      }),
    );

    const theme: ThemeModel = await this.themeModel.create(
      ThemesFactory.create({
        _id: themeId,
        name: 'Test theme',
        source: source._id,
        type: ThemeType.Application,
        versions: [THEME_VERSION_ID],
      }),
    );

    await this.themeVersionModel.create({
      _id: THEME_VERSION_ID,
      theme: THEME_ID,
      source,
    });

    return theme;
  }
}

export = PublishVersionFixture;
