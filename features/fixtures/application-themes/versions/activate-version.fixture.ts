import { getModelToken } from '@nestjs/mongoose';
import { BaseFixture } from '@pe/cucumber-sdk';
import { Model } from 'mongoose';
import { BusinessSchemaName } from '../../../app/schemas';
import { ThemesSchemaNamesEnum, ThemeType } from '../../../../src/themes';
import { ThemesFactory, ThemeSnapshotsFactory, ThemeSourcesFactory } from '../../../fixture-factories';
import { ThemeSnapshotModel, ThemeSourceModel } from '../../../../src/themes/interfaces/entities';

const BUSINESS_ID: string = 'bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb';
const THEME_ID: string = 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa';
const THEME_VERSION_ID: string = 'dddddddd-dddd-dddd-dddd-dddddddddddd';
const SNAPSHOT_ID: string = '22222222-2222-2222-2222-222222222222';
const ANOTHER_SNAPSHOT_ID: string = '33333333-3333-3333-4444-444444444444';
const ANOTHER_THEME_VERSION_ID: string = 'cccccccc-cccc-cccc-cccc-cccccccccccc';

class ActivateVersionFixture extends BaseFixture {

  private businessModel: Model<any> = this.application.get(getModelToken(BusinessSchemaName));
  private themeModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.Theme));
  private themeSourceModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.ThemeSource));
  private themeSnapshotModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.ThemeSnapshot));
  private themeVersionModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.ThemeVersion));

  public async apply(): Promise<void> {
    await this.businessModel.create({
      _id: BUSINESS_ID,
      name: 'Test business',
    });

    await this.createTheme(THEME_ID);
  }

  private async createTheme(themeId: string): Promise<void> {
    const snapshot: ThemeSnapshotModel = await this.themeSnapshotModel
      .create(ThemeSnapshotsFactory.create({_id: SNAPSHOT_ID}));
    const anotherSnapshot: ThemeSnapshotModel = await this.themeSnapshotModel
      .create(ThemeSnapshotsFactory.create({_id: ANOTHER_SNAPSHOT_ID }));

    const source: ThemeSourceModel = await this.themeSourceModel.create(
      ThemeSourcesFactory.create({
        snapshot,
      }),
    );

    const anotherSource: ThemeSourceModel = await this.themeSourceModel.create(
      ThemeSourcesFactory.create({
        snapshot: anotherSnapshot,
      }),
    );

    await this.themeModel.create(
      ThemesFactory.create({
        _id: themeId,
        name: 'Test theme',
        source: source._id,
        type: ThemeType.Application,
        versions: [THEME_VERSION_ID, ANOTHER_THEME_VERSION_ID],
      }),
    );

    await this.themeVersionModel.create({
      _id: THEME_VERSION_ID,
      name: 'test name',
      published: false,
      theme: THEME_ID,
      source,
    });

    await this.themeVersionModel.create({
      _id: ANOTHER_THEME_VERSION_ID,
      name: 'test name',
      published: true,
      theme: THEME_ID,
      source: anotherSource,
      isActive: true,
    });
  }
}

export = ActivateVersionFixture;
