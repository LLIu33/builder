import { getModelToken } from '@nestjs/mongoose';
import { BaseFixture } from '@pe/cucumber-sdk';
import { Model } from 'mongoose';
import { BusinessSchemaName, TestApplicationSchemaName } from '../../../app/schemas';
import { ThemesSchemaNamesEnum, ThemeType } from '../../../../src/themes';
import { ThemesFactory, ThemeSnapshotsFactory, ThemeSourcesFactory } from '../../../fixture-factories';
import { ThemeModel, ThemeSnapshotModel, ThemeSourceModel } from '../../../../src/themes/interfaces/entities';
import { ApplicationThemeSchemaNamesEnum } from '../../../../src/application-theme/enums';

const BUSINESS_ID: string = 'bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb';
const THEME_ID: string = 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa';
const THEME_VERSION_ID: string = 'dddddddd-dddd-dddd-dddd-dddddddddddd';
const APPLICATION_ID: string = 'cccccccc-cccc-cccc-cccc-cccccccccccc';
const SOURCE_ID: string = '11111111-1111-2222-2222-222222222222';
const VERSION_SOURCE_ID: string = '11111111-1111-3333-4444-444444444444';
const SNAPSHOT_ID: string = '22222222-2222-2222-2222-222222222222';
const VERSION_SNAPSHOT_ID: string = '33333333-3333-3333-3333-333333333333';
const SNAPSHOT_HASH = 'aaaaa';
const VERSION_SNAPSHOT_HASH = 'bbbbb';

class RestoreVersionFixture extends BaseFixture {
  private appModel: Model<any> = this.application.get(getModelToken(TestApplicationSchemaName));
  private appThemeModel: Model<any> = this.application
    .get(getModelToken(ApplicationThemeSchemaNamesEnum.ApplicationTheme));
  private businessModel: Model<any> = this.application.get(getModelToken(BusinessSchemaName));
  private themeModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.Theme));
  private themeSourceModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.ThemeSource));
  private themeSnapshotModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.ThemeSnapshot));
  private themeVersionModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.ThemeVersion));

  public async apply(): Promise<void> {

    const appModel: any = await this.appModel.create({
      _id: APPLICATION_ID,
      name: 'Test app',
      business: BUSINESS_ID,
    });

    await this.businessModel.create({
      _id: BUSINESS_ID,
      name: 'Test business',
    });

    const theme: ThemeModel = await this.createTheme(THEME_ID);

    await this.appThemeModel.create({
      application: appModel,
      theme,
    })
  }

  private async createTheme(themeId: string): Promise<ThemeModel> {
    const snapshot: ThemeSnapshotModel = await this.themeSnapshotModel
      .create(ThemeSnapshotsFactory.create({_id: SNAPSHOT_ID, hash: SNAPSHOT_HASH }));

    const source: ThemeSourceModel = await this.themeSourceModel.create(
      ThemeSourcesFactory.create({
        _id: SOURCE_ID,
        actions: [
          {id: 'first_action'},
          {id: 'second_action'},
        ],
        snapshot,
      }),
    );

    const versionSnapshot: ThemeSnapshotModel = await this.themeSnapshotModel
      .create(ThemeSnapshotsFactory.create({ _id: VERSION_SNAPSHOT_ID, hash: VERSION_SNAPSHOT_HASH }));

    const versionSource: ThemeSourceModel = await this.themeSourceModel.create(
      ThemeSourcesFactory.create({
        _id: VERSION_SOURCE_ID,
        actions: [
          {id: 'first_action'},
        ],
        snapshot: versionSnapshot,
      }),
    );

    const theme: ThemeModel = await this.themeModel.create(
      ThemesFactory.create({
        _id: themeId,
        name: 'Test theme',
        source: source._id,
        type: ThemeType.Application,
        versions: [THEME_VERSION_ID],
      }),
    );

    await this.themeVersionModel.create({
      _id: THEME_VERSION_ID,
      theme: THEME_ID,
      source: versionSource,
    });

    return theme;
  }
}

export = RestoreVersionFixture;
