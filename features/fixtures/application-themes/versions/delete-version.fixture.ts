import { getModelToken } from '@nestjs/mongoose';
import { BaseFixture } from '@pe/cucumber-sdk';
import { Model } from 'mongoose';
import { BusinessSchemaName } from '../../../app/schemas';
import { ThemesSchemaNamesEnum, ThemeType } from '../../../../src/themes';
import { ThemesFactory, ThemeSnapshotsFactory, ThemeSourcesFactory } from '../../../fixture-factories';
import { ThemeSnapshotModel, ThemeSourceModel } from '../../../../src/themes/interfaces/entities';

const BUSINESS_ID: string = 'bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb';
const THEME_ID: string = 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa';
const THEME_VERSION_ID: string = 'dddddddd-dddd-dddd-dddd-dddddddddddd';

class DeleteVersionFixture extends BaseFixture {

  private businessModel: Model<any> = this.application.get(getModelToken(BusinessSchemaName));
  private themeModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.Theme));
  private themeSourceModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.ThemeSource));
  private themeSnapshotModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.ThemeSnapshot));
  private themeVersionModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.ThemeVersion));

  public async apply(): Promise<void> {
    await this.businessModel.create({
      _id: BUSINESS_ID,
      name: 'Test business',
    });

    await this.createTheme(THEME_ID);
  }

  private async createTheme(themeId: string): Promise<void> {
    const snapshot: ThemeSnapshotModel = await this.themeSnapshotModel.create(ThemeSnapshotsFactory.create({ }));

    const source: ThemeSourceModel = await this.themeSourceModel.create(
      ThemeSourcesFactory.create({
        snapshot,
      }),
    );

    await this.themeModel.create(
      ThemesFactory.create({
        _id: themeId,
        name: 'Test theme',
        source: source._id,
        type: ThemeType.Application,
        versions: [THEME_VERSION_ID],
      }),
    );

    await this.themeVersionModel.create({
      _id: THEME_VERSION_ID,
      theme: THEME_ID,
      source,
    });
  }
}

export = DeleteVersionFixture;
