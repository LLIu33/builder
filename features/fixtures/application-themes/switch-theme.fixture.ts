import { getModelToken } from '@nestjs/mongoose';
import { BaseFixture } from '@pe/cucumber-sdk';
import { Model } from 'mongoose';
import { BusinessSchemaName, TestApplicationSchemaName } from '../../app/schemas';
import { ThemesSchemaNamesEnum } from '../../../src/themes';
import { ThemeSnapshotModel, ThemeSourceModel } from '../../../src/themes/interfaces/entities';
import { ThemesFactory, ThemeSnapshotsFactory, ThemeSourcesFactory } from '../../fixture-factories';
import { ThemeType } from '../../../src/themes/enums';
import { ApplicationThemeSchemaNamesEnum } from '../../../src/application-theme/enums';

const APPLICATION_ID: string = 'cccccccc-cccc-cccc-cccc-cccccccccccc';
const BUSINESS_ID: string = 'bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb';
const ANOTHER_BUSINESS_ID: string = 'aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa';
const THEME_ID: string = 'dddddddd-dddd-dddd-dddd-dddddddddddd';

class SwitchThemeFixture extends BaseFixture {
  private appModel: Model<any> = this.application.get(getModelToken(TestApplicationSchemaName));
  private businessModel: Model<any> = this.application.get(getModelToken(BusinessSchemaName));


  private themeModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.Theme));
  private themeSourceModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.ThemeSource));
  private themeSnapshotModel: Model<any> = this.application.get(getModelToken(ThemesSchemaNamesEnum.ThemeSnapshot));

  private appThemeModel: Model<any> = this.application
    .get(getModelToken(ApplicationThemeSchemaNamesEnum.ApplicationTheme));

  public async apply(): Promise<void> {
    await this.appModel.create({
      _id: APPLICATION_ID,
      name: 'Test app',
      business: BUSINESS_ID,
    });

    await this.businessModel.create({
      _id: BUSINESS_ID,
      name: 'Test business',
    });

    await this.businessModel.create({
      _id: ANOTHER_BUSINESS_ID,
      name: 'Another business',
    });

    await this.createTemplate(THEME_ID);

    await this.appThemeModel.create({
      _id: '11111111-1111-1111-1111-111111111111',
      application: APPLICATION_ID,
      isActive: true,
      theme: 'bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb',
    });

    await this.appThemeModel.create({
      _id: '22222222-2222-2222-2222-222222222222',
      application: APPLICATION_ID,
      isActive: false,
      theme: THEME_ID,
    });
  }

  private async createTemplate(themeId: string): Promise<void> {
    const snapshot: ThemeSnapshotModel = await this.themeSnapshotModel.create(ThemeSnapshotsFactory.create({ }));

    const source: ThemeSourceModel = await this.themeSourceModel.create(
      ThemeSourcesFactory.create({
        snapshot,
      }),
    );

    await this.themeModel.create(
      ThemesFactory.create({
        _id: themeId,
        name: 'Test theme',
        source: source._id,
        type: ThemeType.Application,
      }),
    );
  }
}

export = SwitchThemeFixture;
