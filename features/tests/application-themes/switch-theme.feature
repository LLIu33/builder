Feature: Switch theme
  Background:
    And I remember as "businessId" following value:
      """
      "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb"
      """
    And I remember as "anotherBusinessId" following value:
      """
      "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
      """
    And I remember as "applicationId" following value:
      """
      "cccccccc-cccc-cccc-cccc-cccccccccccc"
      """
    And I remember as "templateId" following value:
      """
      "dddddddd-dddd-dddd-dddd-dddddddddddd"
      """
    Given I authenticate as a user with the following data:
      """
      {
        "email": "email@email.com",
        "roles": [
          {
            "name": "merchant",
            "permissions": [
              {
                "businessId": "{{businessId}}",
                "acls": []
              }
            ]
          }
        ]
      }
      """

  Scenario: Switch theme
    Given I use DB fixture "application-themes/switch-theme"
    When I send a PUT request to "/api/business/{{businessId}}/application/{{applicationId}}/theme/{{templateId}}/switch"
    Then print last response
    And the response status code should be 200
    And the response should contain json:
      """
      {
        "_id": "{{templateId}}",
        "source": "*",
        "type": "application",
        "id": "*"
      }
      """
    And I store a response as "response"
    And model "Theme" with id "{{response.id}}" should contain json:
      """
      {
        "type": "application"
      }
      """
    And I look for model "ApplicationTheme" by following JSON and remember as "appTheme":
      """
      {
        "theme": "{{response.id}}",
        "application": "{{applicationId}}"
      }
      """
    And stored value "appTheme" should contain json:
      """
      {
        "theme": "{{templateId}}",
        "application": "{{applicationId}}",
        "isActive": true
      }
      """
    And model "ApplicationTheme" with id "11111111-1111-1111-1111-111111111111" should contain json:
      """
      {
        "application": "{{applicationId}}",
        "isActive": false
      }
      """

  Scenario: Switch theme, app theme not exists
    Given I use DB fixture "application-themes/switch-theme-app-theme-not-exists"
    When I send a PUT request to "/api/business/{{businessId}}/application/{{applicationId}}/theme/{{templateId}}/switch"
    Then print last response
    And the response status code should be 404
    And the response should contain json:
      """
      {
        "message": "There is no theme \"{{templateId}}\" for application \"{{applicationId}}\"."
      }
      """

  Scenario: Switch theme of another business
    Given I authenticate as a user with the following data:
      """
      {
        "email": "email@email.com",
        "roles": [
          {
            "name": "merchant",
            "permissions": [
              {
                "businessId": "{{anotherBusinessId}}",
                "acls": []
              }
            ]
          }
        ]
      }
      """
    And I use DB fixture "application-themes/switch-theme"
    When I send a PUT request to "/api/business/{{anotherBusinessId}}/application/{{applicationId}}/theme/{{templateId}}/switch"
    Then print last response
    And the response status code should be 403
