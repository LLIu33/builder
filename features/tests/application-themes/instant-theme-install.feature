Feature: Instant theme install
  Background:
    And I remember as "businessId" following value:
      """
      "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb"
      """
    And I remember as "applicationId" following value:
      """
      "cccccccc-cccc-cccc-cccc-cccccccccccc"
      """
    And I remember as "templateId" following value:
      """
      "dddddddd-dddd-dddd-dddd-dddddddddddd"
      """
    And I remember as "themeId" following value:
      """
      "1111dddd-dddd-dddd-dddd-dddddddddddd"
      """
    Given I authenticate as a user with the following data:
      """
      {
        "email": "email@email.com",
        "roles": [
          {
            "name": "merchant",
            "permissions": [
              {
                "businessId": "{{businessId}}",
                "acls": []
              }
            ]
          }
        ]
      }
      """

  Scenario: Instant theme install
    Given I use DB fixture "application-themes/instant-theme-install"
    When I send a PUT request to "/api/business/{{businessId}}/application/{{applicationId}}/template/{{templateId}}/instant-setup"
    Then print last response
    And the response status code should be 200
    And the response should contain json:
      """
      {
           "_id": "*",
           "source": "*",
           "type": "application",
           "id": "*"
         }
      """
    And I store a response as "response"
    And model "Theme" with id "{{response.id}}" should contain json:
      """
      {
        "type": "application"
      }
      """
    And I look for model "ApplicationTheme" by following JSON and remember as "appTheme":
      """
      {
        "theme": "{{response.id}}",
        "application": "{{applicationId}}",
        "isActive": true
      }
      """
    And stored value "appTheme" should contain json:
      """
      {
        "theme": "{{response.id}}",
        "application": "{{applicationId}}"
      }
      """
    And print RabbitMQ message list
    And RabbitMQ exchange "async_events" should contain following ordered messages:
      """
      [
        {
          "name": "builder.event.template.installed",
          "payload": {
            "installedTheme": {
              "id": "*"
            }
          }
        }
      ]
      """

  Scenario: publish version by rabbit message
    Given I use DB fixture "application-themes/publish-version"
    When I publish in RabbitMQ channel "async_events_test_micro" message with json:
      """
        {
          "name": "builder.event.template.installed",
          "payload": {
            "installedTheme": {
              "id": "{{themeId}}"
            }
          }
        }
      """
    Then I process messages from RabbitMQ "async_events_test_micro" channel
    And RabbitMQ exchange "async_events" should contain following ordered messages:
    """
    [
      {
        "name": "builder.event.theme.published",
        "payload": {
          "application": {
            "type": "shop"
          },
          "compiled": {
            "theme": {
              "_id": "*",
              "context": "*",
              "data": "*",
              "routing": "*",
              "application": "{{applicationId}}"
            },
            "themePages": [
              "*"
            ]
          },
          "isDeployed": true,
          "theme": {}
        }
      }
    ]
    """
