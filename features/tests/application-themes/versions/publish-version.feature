Feature: Publish theme version
  Background:
    And I remember as "themeId" following value:
      """
      "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
      """
    And I remember as "businessId" following value:
      """
      "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb"
      """
    And I remember as "applicationId" following value:
      """
      "cccccccc-cccc-cccc-cccc-cccccccccccc"
      """
    And I remember as "versionId" following value:
      """
      "dddddddd-dddd-dddd-dddd-dddddddddddd"
      """
    And I remember as "pageId" following value:
      """
      "11111111-1111-1111-1111-111111111111"
      """
    Given I authenticate as a user with the following data:
      """
      {
        "email": "email@email.com",
        "roles": [
          {
            "name": "merchant",
            "permissions": [
              {
                "businessId": "{{businessId}}",
                "acls": []
              }
            ]
          }
        ]
      }
      """

  Scenario: Publish theme version
    Given I use DB fixture "application-themes/versions/publish-version"
    When I send a PUT request to "/api/theme/{{themeId}}/version/{{versionId}}/publish"
    Then print last response
    And the response status code should be 200
    And I store a response as "response"
    And the response should contain json:
      """
      {
        "_id": "{{versionId}}",
        "theme": "{{themeId}}",
        "source": "*",
        "published": true
      }
      """
    And model "ThemeVersion" with id "{{versionId}}" should contain json:
      """
      {
        "published": true
      }
      """
    And print RabbitMQ message list
    And RabbitMQ exchange "async_events" should contain following ordered messages:
      """
      [
        {
          "name": "builder.event.theme.published",
          "payload": {
            "application": {
              "type": "shop"
            },
            "compiled": {
              "theme": {
                "pages": "{{pageId}}",
                "_id": "*",
                "context": "*",
                "data": "*",
                "routing": "*",
                "application": "{{applicationId}}"
              },
              "themePages": [
                "*"
              ]
            },
            "isDeployed": true,
            "theme": {}
          }
        }
      ]
      """

