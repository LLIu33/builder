Feature: Get theme version
  Background:
    And I remember as "themeId" following value:
      """
      "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
      """
    And I remember as "versionId" following value:
      """
      "dddddddd-dddd-dddd-dddd-dddddddddddd"
      """
    And I remember as "anotherVersionId" following value:
      """
      "cccccccc-cccc-cccc-cccc-cccccccccccc"
      """
    And I remember as "snapshotId" following value:
      """
      "22222222-2222-2222-2222-222222222222"
      """
    And I remember as "anotherSnapshotId" following value:
      """
      "33333333-3333-3333-4444-444444444444"
      """
    Given I authenticate as a user with the following data:
      """
      {
        "email": "email@email.com",
        "roles": [
          {
            "name": "merchant",
            "permissions": [
              {
                "businessId": "{{businessId}}",
                "acls": []
              }
            ]
          }
        ]
      }
      """

  Scenario: Get list of theme versions request
    Given I use DB fixture "application-themes/versions/get-version"
    When I send a GET request to "/api/theme/{{themeId}}/versions"
    Then print last response
    And the response status code should be 200
    And I store a response as "response"
    And the response should contain json:
      """
      [{
        "name": "test name",
        "published": false,
        "theme": "{{themeId}}",
        "source": "*",
        "id": "{{versionId}}"
      },
      {
        "name": "test name",
        "published": true,
        "theme": "{{themeId}}",
        "source": "*",
        "id": "{{anotherVersionId}}"
      }]
      """

  Scenario: Get theme version request
    Given I use DB fixture "application-themes/versions/get-version"
    When I send a GET request to "/api/theme/{{themeId}}/version/{{versionId}}"
    Then print last response
    And the response status code should be 200
    And I store a response as "response"
    And the response should contain json:
      """
      {
        "name": "Test theme",
        "picture": "*",
        "publishedId": "{{anotherVersionId}}",
        "source": {},
        "sourceId": "*",
        "id": "{{themeId}}",
        "versionsIds": ["{{versionId}}", "{{anotherVersionId}}"]
      }
      """

  Scenario: Get snapshot for theme version request
    Given I use DB fixture "application-themes/versions/get-version"
    When I send a GET request to "/api/theme/{{themeId}}/version/{{versionId}}/snapshot"
    Then print last response
    And the response status code should be 200
    And I store a response as "response"
    And the response should contain json:
      """
      {
        "_id": "{{snapshotId}}"
      }
      """
    When I send a GET request to "/api/theme/{{themeId}}/version/{{anotherVersionId}}/snapshot"
    Then print last response
    And the response status code should be 200
    And I store a response as "response"
    And the response should contain json:
      """
      {
        "_id": "{{anotherSnapshotId}}"
      }
      """
