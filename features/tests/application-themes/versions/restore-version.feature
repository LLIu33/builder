Feature: Publish theme version
  Background:
    And I remember as "themeId" following value:
      """
      "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
      """
    And I remember as "businessId" following value:
      """
      "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb"
      """
    And I remember as "applicationId" following value:
      """
      "cccccccc-cccc-cccc-cccc-cccccccccccc"
      """
    And I remember as "versionId" following value:
      """
      "dddddddd-dddd-dddd-dddd-dddddddddddd"
      """
    And I remember as "snapshotId" following value:
      """
      "22222222-2222-2222-2222-222222222222"
      """
    And I remember as "versionSnapshotId" following value:
      """
      "33333333-3333-3333-3333-333333333333"
      """
    And I remember as "sourceId" following value:
      """
      "11111111-1111-2222-2222-222222222222"
      """
    And I remember as "versionSourceId" following value:
      """
      "11111111-1111-3333-4444-444444444444"
      """
    And I remember as "snapshotHash" following value:
      """
      "aaaaa"
      """
    And I remember as "versionSnapshotHash" following value:
      """
      "bbbbb"
      """
    Given I authenticate as a user with the following data:
      """
      {
        "email": "email@email.com",
        "roles": [
          {
            "name": "merchant",
            "permissions": [
              {
                "businessId": "{{businessId}}",
                "acls": []
              }
            ]
          }
        ]
      }
      """

  Scenario: Publish theme version
    Given I use DB fixture "application-themes/versions/restore-version"
    When I send a PUT request to "/api/theme/{{themeId}}/version/{{versionId}}/restore"
    Then print last response
    And the response status code should be 200
    And I store a response as "response"
    And the response should contain json:
      """
      {
        "_id": "{{versionId}}",
        "theme": "{{themeId}}",
        "source": "{{versionSourceId}}",
        "isActive": true
      }
      """
    And model "ThemeSource" with id "{{sourceId}}" should not contain json:
      """
      {
        "actions": [
          {"id": "second_action"}
        ]
      }
      """
    And model "ThemeSnapshot" with id "{{snapshotId}}" should contain json:
      """
      {
        "hash": "{{versionSnapshotHash}}"
      }
      """
