Feature: Create theme version
  Background:
    And I remember as "themeId" following value:
      """
      "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
      """
    And I remember as "businessId" following value:
      """
      "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb"
      """
    And I remember as "applicationId" following value:
      """
      "cccccccc-cccc-cccc-cccc-cccccccccccc"
      """
    And I remember as "existVersionId" following value:
      """
      "dddddddd-dddd-dddd-dddd-dddddddddddd"
      """
    Given I authenticate as a user with the following data:
      """
      {
        "email": "email@email.com",
        "roles": [
          {
            "name": "merchant",
            "permissions": [
              {
                "businessId": "{{businessId}}",
                "acls": []
              }
            ]
          }
        ]
      }
      """

  Scenario: Create theme version request
    Given I use DB fixture "application-themes/versions/create-version"
    When I send a POST request to "/api/theme/{{themeId}}/version" with json:
      """
      {
        "name": "test version"
      }
      """
    Then print last response
    And the response status code should be 201
    And I store a response as "response"
    And the response should contain json:
      """
      {
        "_id": "*",
        "name": "test version",
        "published": false,
        "isActive": true,
        "source": "*",
        "theme": "{{themeId}}"
      }
      """
    And model "Theme" with id "{{themeId}}" should contain json:
      """
      {
        "versions": ["{{response._id}}"]
      }
      """
    And model "ThemeVersion" with id "{{existVersionId}}" should contain json:
      """
      {
        "isActive": false
      }
      """
