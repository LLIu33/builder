Feature: Delete theme version
  Background:
    And I remember as "themeId" following value:
      """
      "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
      """
    And I remember as "businessId" following value:
      """
      "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb"
      """
    And I remember as "applicationId" following value:
      """
      "cccccccc-cccc-cccc-cccc-cccccccccccc"
      """
    And I remember as "versionId" following value:
      """
      "dddddddd-dddd-dddd-dddd-dddddddddddd"
      """
    Given I authenticate as a user with the following data:
      """
      {
        "email": "email@email.com",
        "roles": [
          {
            "name": "merchant",
            "permissions": [
              {
                "businessId": "{{businessId}}",
                "acls": []
              }
            ]
          }
        ]
      }
      """

  Scenario: Delete theme version request
    Given I use DB fixture "application-themes/versions/delete-version"
    When I send a DELETE request to "/api/theme/{{themeId}}/version/{{versionId}}"
    Then print last response
    And the response status code should be 200
    And I store a response as "response"
    And the response should contain json:
      """
      {
        "status": "ok"
      }
      """
    And model "Theme" with id "{{themeId}}" should not contain json:
      """
      {
        "versions": ["{{versionId}}"]
      }
      """
    And model "ThemeVersion" with id "{{versionId}}" should not exist

