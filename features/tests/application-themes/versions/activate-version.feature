Feature: Publish theme version
  Background:
    And I remember as "themeId" following value:
      """
      "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
      """
    And I remember as "versionId" following value:
      """
      "dddddddd-dddd-dddd-dddd-dddddddddddd"
      """
    And I remember as "anotherVersionId" following value:
      """
      "cccccccc-cccc-cccc-cccc-cccccccccccc"
      """
    Given I authenticate as a user with the following data:
      """
      {
        "email": "email@email.com",
        "roles": [
          {
            "name": "merchant",
            "permissions": [
              {
                "businessId": "{{businessId}}",
                "acls": []
              }
            ]
          }
        ]
      }
      """

  Scenario: Activate theme version
    Given I use DB fixture "application-themes/versions/activate-version"
    When I send a PATCH request to "/api/theme/{{themeId}}/version/{{versionId}}/active"
    Then print last response
    And the response status code should be 200
    And I store a response as "response"
    And the response should contain json:
      """
      {
        "_id": "{{versionId}}",
        "theme": "{{themeId}}",
        "source": "*",
        "isActive": true
      }
      """
    And model "ThemeVersion" with id "{{versionId}}" should contain json:
      """
      {
        "isActive": true
      }
      """

  Scenario: Get active theme
    Given I use DB fixture "application-themes/versions/activate-version"
    When I send a GET request to "/api/theme/{{themeId}}/version/active"
    Then print last response
    And the response status code should be 200
    And I store a response as "response"
    And the response should contain json:
      """
      {
        "_id": "{{anotherVersionId}}",
        "theme": "{{themeId}}",
        "source": "*",
        "isActive": true
      }
      """

  Scenario: Get nonexist active theme
  Given I use DB fixture "application-themes/versions/get-version"
    When I send a GET request to "/api/theme/{{themeId}}/version/active"
    Then print last response
    And the response status code should be 200
    And I store a response as "response"
    And the response should not contain json:
      """
      {
        "theme": "{{themeId}}"
      }
      """
