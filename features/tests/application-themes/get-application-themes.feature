Feature: Get application themes
  Background:
    And I remember as "businessId" following value:
      """
      "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb"
      """
    And I remember as "anotherBusinessId" following value:
      """
      "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
      """
    And I remember as "applicationId" following value:
      """
      "cccccccc-cccc-cccc-cccc-cccccccccccc"
      """
    And I remember as "anotherApplicationId" following value:
      """
      "cccccccc-cccc-cccc-cccc-222222222222"
      """
    And I remember as "anotherAppTheme" following value:
      """
      "44444444-4444-4444-4444-444444444444"
      """
    And I remember as "activeThemeId" following value:
      """
      "55555555-5555-5555-5555-555555555555"
      """
    Given I authenticate as a user with the following data:
      """
      {
        "email": "email@email.com",
        "roles": [
          {
            "name": "admin",
            "permissions": [
              {
                "businessId": "{{businessId}}",
                "acls": []
              }
            ]
          }
        ]
      }
      """

  Scenario: Get all themes
    Given I use DB fixture "application-themes/app-themes-list"
    When I send a GET request to "/api/business/{{businessId}}/application/{{applicationId}}/themes"
    Then print last response
    And the response status code should be 200
    And the response should contain json:
      """
      [
        {
          "_id": "*",
          "id": "*",
          "theme": {
            "versions": [
              "*"
            ],
            "_id": "11111111-1111-1111-1111-111111111111",
            "name": "*",
            "source": "*",
            "isDefault": false,
            "picture": "*",
            "publishedVersion": "*",
            "type": "template",
            "id": "11111111-1111-1111-1111-111111111111"
          },
          "application": "{{applicationId}}",
          "isActive": false
        },
        {
          "theme": {
            "_id": "22222222-2222-2222-2222-222222222222"
          }
        },
        {
          "theme": {
            "_id": "33333333-3333-3333-4444-444444444444"
          }
        },
        {
          "theme": {
            "_id": "55555555-5555-5555-5555-555555555555"
          }
        }
      ]
      """
    And the response should not contain json:
      """
      [
        {
          "application": "{{anotherApplicationId}}"
        }
      ]
      """

  Scenario: Get active theme
    Given I use DB fixture "application-themes/app-themes-list"
    When I send a GET request to "/api/business/{{businessId}}/application/{{applicationId}}/themes/active"
    Then print last response
    And the response status code should be 200
    And the response should contain json:
      """
      {
        "_id": "*",
        "theme": "{{activeThemeId}}",
        "application": "{{applicationId}}",
        "isActive": true
      }
      """

  Scenario: Get theme preview
    Given I use DB fixture "application-themes/app-themes-list"
    When I send a GET request to "/api/business/{{businessId}}/application/{{applicationId}}/preview"
    Then print last response
    And the response status code should be 200
    And the response should contain json:
      """
      {
        "current": {
          "_id": "*",
          "pages": {
            "11111111-1111-1111-1111-111111111111": {
              "id": "11111111-1111-1111-1111-111111111111",
              "variant": "front",
              "templateId": "*"
            }
          }
        },
        "published": {
           "pages": [
              {"variant": "front"},
              {"variant": "about"}
           ]
        }
      }
      """

  Scenario: Get theme preview
    Given I use DB fixture "application-themes/app-themes-list"
    When I send a GET request to "/api/business/{{businessId}}/application/{{applicationId}}/preview?include=published&page=front"
    Then print last response
    And the response status code should be 200
    And the response should contain json:
      """
      {
        "published": {
          "pages": [
            {
              "variant": "front"
            }
          ]
        }
      }
      """
    And the response should not contain json:
      """
      {
        "published": {
          "pages": [
            {
              "variant": "about"
            }
          ]
        }
      }
      """

