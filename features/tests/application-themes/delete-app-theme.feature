Feature: Delete app theme
  Background:
    And I remember as "businessId" following value:
      """
      "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb"
      """
    And I remember as "anotherBusinessId" following value:
      """
      "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
      """
    And I remember as "applicationId" following value:
      """
      "cccccccc-cccc-cccc-cccc-cccccccccccc"
      """
    And I remember as "themeId" following value:
      """
      "dddddddd-dddd-dddd-dddd-dddddddddddd"
      """
    Given I authenticate as a user with the following data:
      """
      {
        "email": "email@email.com",
        "roles": [
          {
            "name": "merchant",
            "permissions": [
              {
                "businessId": "{{businessId}}",
                "acls": []
              }
            ]
          }
        ]
      }
      """

  Scenario: Delete app theme
    Given I use DB fixture "application-themes/delete-app-theme"
    When I send a DELETE request to "/api/business/{{businessId}}/application/{{applicationId}}/theme/{{themeId}}"
    Then print last response
    And the response status code should be 200
    And the response should contain json:
      """
      {
        "status": "ok"
      }
      """
    And I store a response as "response"
    And model "ApplicationTheme" found by following JSON should not exist:
      """
      {
        "theme": "{{themeId}}",
        "application": "{{applicationId}}"
      }
      """
    And model "Theme" with id "{{themeId}}" should not exist

  Scenario: Delete active app theme
    Given I use DB fixture "application-themes/delete-active-app-theme"
    When I send a DELETE request to "/api/business/{{businessId}}/application/{{applicationId}}/theme/{{themeId}}"
    Then print last response
    And the response status code should be 409
    And the response should contain json:
      """
      {
        "statusCode": 409,
        "error": "Conflict",
        "message": "Theme with id \"{{themeId}}\" is active for application \"{{applicationId}}\" and can not be removed."
      }
      """

  Scenario: Delete app theme, not related with app
    Given I use DB fixture "application-themes/delete-app-theme-not-exists"
    When I send a DELETE request to "/api/business/{{businessId}}/application/{{applicationId}}/theme/{{themeId}}"
    Then print last response
    And the response status code should be 404
    And the response should contain json:
      """
      {
        "message": "There is no theme \"{{themeId}}\" for application \"{{applicationId}}\"."
      }
      """

  Scenario: Delete app theme, of another business
    Given I authenticate as a user with the following data:
      """
      {
        "email": "email@email.com",
        "roles": [
          {
            "name": "merchant",
            "permissions": [
              {
                "businessId": "{{anotherBusinessId}}",
                "acls": []
              }
            ]
          }
        ]
      }
      """
    Given I use DB fixture "application-themes/delete-app-theme"
    When I send a DELETE request to "/api/business/{{anotherBusinessId}}/application/{{applicationId}}/theme/{{themeId}}"
    Then print last response
    And the response status code should be 403
