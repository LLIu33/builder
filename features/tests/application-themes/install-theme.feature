Feature: Install theme
  Background:
    And I remember as "businessId" following value:
      """
      "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb"
      """
    And I remember as "anotherBusinessId" following value:
      """
      "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
      """
    And I remember as "applicationId" following value:
      """
      "cccccccc-cccc-cccc-cccc-cccccccccccc"
      """
    And I remember as "templateId" following value:
      """
      "dddddddd-dddd-dddd-dddd-dddddddddddd"
      """
    Given I authenticate as a user with the following data:
      """
      {
        "email": "email@email.com",
        "roles": [
          {
            "name": "merchant",
            "permissions": [
              {
                "businessId": "{{businessId}}",
                "acls": []
              }
            ]
          }
        ]
      }
      """

  Scenario: Theme install
    Given I use DB fixture "application-themes/theme-install"
    When I send a POST request to "/api/business/{{businessId}}/application/{{applicationId}}/theme/{{templateId}}/install"
    Then print last response
    And the response status code should be 201
    And the response should contain json:
      """
      {
        "_id": "*",
        "source": "*",
        "type": "application",
        "name": "Test theme",
        "id": "*"
      }
      """
    And I store a response as "response"
    And model "Theme" with id "{{response.id}}" should contain json:
      """
      {
        "type": "application",
        "name": "Test theme"
      }
      """
    And I look for model "ApplicationTheme" by following JSON and remember as "appTheme":
      """
      {
        "theme": "{{response.id}}",
        "application": "{{applicationId}}",
        "isActive": true
      }
      """
    And stored value "appTheme" should contain json:
      """
      {
        "theme": "{{response.id}}",
        "application": "{{applicationId}}"
      }
      """
    And I look for model "CompiledTheme" by following JSON and remember as "compiledTheme":
      """
      {
        "application": "{{applicationId}}"
      }
      """

  Scenario: Theme install for another business
    Given I authenticate as a user with the following data:
      """
      {
        "email": "email@email.com",
        "roles": [
          {
            "name": "merchant",
            "permissions": [
              {
                "businessId": "{{anotherBusinessId}}",
                "acls": []
              }
            ]
          }
        ]
      }
      """
    Given I use DB fixture "application-themes/theme-install"
    When I send a POST request to "/api/business/{{anotherBusinessId}}/application/{{applicationId}}/theme/{{templateId}}/install"
    Then print last response
    And the response status code should be 403
