Feature: Create theme template
  Background:
    And I remember as "themeId" following value:
      """
      "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
      """
    And I remember as "groupId" following value:
      """
      "dddddddd-dddd-dddd-dddd-dddddddddddd"
      """
    And I remember as "templateThemeItemId" following value:
      """
      "ffffffff-ffff-ffff-ffff-ffffffffffff"
      """
    Given I authenticate as a user with the following data:
      """
      {
        "email": "email@email.com",
        "roles": [
          {
            "name": "merchant",
            "permissions": [
              {
                "businessId": "{{businessId}}",
                "acls": []
              }
            ]
          }
        ]
      }
      """

  Scenario: Mark theme as template
    Given I use DB fixture "template-themes/get-templates"
    When I send a GET request to "/api/templates"
    Then print last response
    And the response status code should be 200
    And I store a response as "response"
    And the response should contain json:
      """
      [
        {
          "_id": "{{groupId}}",
          "code": "testGroup",
          "items": [
            {
              "_id": "{{templateThemeItemId}}",
              "themes": [
                {
                  "_id": "{{themeId}}"
                }
              ]
            }
          ]
        }
      ]
      """
