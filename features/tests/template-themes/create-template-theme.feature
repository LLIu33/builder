Feature: Create theme template
  Background:
    And I remember as "themeId" following value:
      """
      "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
      """
    And I remember as "businessId" following value:
      """
      "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
      """
    And I remember as "templateThemeGroupId" following value:
      """
      "dddddddd-dddd-dddd-dddd-dddddddddddd"
      """
    And I remember as "templateThemeItemId" following value:
      """
      "ffffffff-ffff-ffff-ffff-ffffffffffff"
      """
    Given I authenticate as a user with the following data:
      """
      {
        "email": "email@email.com",
        "roles": [
          {
            "name": "merchant",
            "permissions": [
              {
                "businessId": "{{businessId}}",
                "acls": []
              }
            ]
          }
        ]
      }
      """

  Scenario: Mark theme as template
    Given I use DB fixture "template-themes/mark-theme-as-template"
    When I send a POST request to "/api/{{themeId}}/template" with json:
      """
      {
        "codeGroup": "testGroup",
        "order": 1,
        "codeItem": "testItem",
        "type": "product"
      }
      """
    Then print last response
    And the response status code should be 201
    And I look for model "TemplateThemeItem" by following JSON and remember as "templateItem":
      """
      {
        "code": "testItem"
      }
      """
    And stored value "templateItem" should contain json:
      """
      {
        "code": "testItem",
        "themes": ["{{themeId}}"]
      }
      """
    And I look for model "TemplateThemeGroup" by following JSON and remember as "templateGroup":
      """
      {
        "code": "testGroup"
      }
      """
    And stored value "templateGroup" should contain json:
      """
      {
        "code": "testGroup",
        "items": ["{{templateItem._id}}"]
      }
      """

  Scenario: Mark theme as template when group exists
    Given I use DB fixture "template-themes/mark-theme-as-template-group-exists"
    When I send a POST request to "/api/{{themeId}}/template" with json:
      """
      {
        "codeGroup": "testGroup",
        "order": 1,
        "codeItem": "testItem",
        "type": "product"
      }
      """
    Then print last response
    And the response status code should be 201
    And I look for model "TemplateThemeItem" by following JSON and remember as "templateItem":
      """
      {
        "code": "testItem"
      }
      """
    And stored value "templateItem" should contain json:
      """
      {
        "code": "testItem",
        "themes": ["{{themeId}}"]
      }
      """
      And model "TemplateThemeGroup" with id "{{templateThemeGroupId}}" should contain json:
      """
      {
        "code": "testGroup",
        "items": ["{{templateItem._id}}"]
      }
      """

  Scenario: Mark application theme as template
    Given I use DB fixture "template-themes/mark-application-theme-as-template"
    When I send a POST request to "/api/{{themeId}}/template" with json:
      """
      {
        "codeGroup": "testGroup",
        "order": 1,
        "codeItem": "testItem",
        "type": "product"
      }
      """
    Then print last response
    And the response status code should be 405
