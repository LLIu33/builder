Feature: Get theme
  Background:
    And I remember as "themeId" following value:
      """
      "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
      """
    And I remember as "pageId" following value:
      """
      "11111111-1111-1111-1111-111111111111"
      """
    And I remember as "anothePageId" following value:
      """
      "22222222-2222-2222-2222-222222222222"
      """
    And I remember as "businessId" following value:
      """
      "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb"
      """
    And I remember as "snapshotId" following value:
      """
      "ffffffff-ffff-ffff-ffff-ffffffffffff"
      """
    Given I authenticate as a user with the following data:
      """
      {
        "email": "email@email.com",
        "roles": [
          {
            "name": "merchant",
            "permissions": [
              {
                "businessId": "{{businessId}}",
                "acls": []
              }
            ]
          }
        ]
      }
      """

  Scenario: Get actual snapshot
    Given I use DB fixture "themes/theme-exists"
    When I send a GET request to "/api/theme/{{themeId}}/snapshot"
    Then print last response
    And the response status code should be 200
    And the response should contain json:
      """
      {
        "_id": "{{snapshotId}}"
      }
      """

  Scenario: Get actual snapshot pages
    Given I use DB fixture "themes/snapshot-already-compiled"
    When I send a GET request to "/api/theme/{{themeId}}/pages"
    Then print last response
    And the response status code should be 200
    And the response should contain json:
      """
      {
        "{{pageId}}": {
          "id": "{{pageId}}",
          "variant": "front",
          "name": "Main Page"
        }
      }
      """

  Scenario: Get partial snapshot
    Given I use DB fixture "themes/snapshot-with-many-pages"
    When I send a GET request to "/api/theme/{{themeId}}/snapshot/{{pageId}}"
    Then print last response
    And the response status code should be 200
    And the response should contain json:
      """
      {
        "_id": "{{snapshotId}}",
        "pages": {
          "{{pageId}}": {
            "id": "{{pageId}}"
          }
        }
      }
      """
    And the response should not contain json:
      """
      {
        "pages": {
          "{{anothePageId}}": {
            "id": "{{anothePageId}}"
          }
        }
      }
      """
 