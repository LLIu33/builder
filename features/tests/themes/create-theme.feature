Feature: Create theme
  Background:
    And I remember as "themeId" following value:
      """
      "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
      """
    And I remember as "pageId" following value:
      """
      "11111111-1111-1111-1111-111111111111"
      """
    And I remember as "businessId" following value:
      """
      "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb"
      """
    Given I authenticate as a user with the following data:
      """
      {
        "email": "email@email.com",
        "roles": [
          {
            "name": "merchant",
            "permissions": [
              {
                "businessId": "{{businessId}}",
                "acls": []
              }
            ]
          }
        ]
      }
      """

  Scenario: Create theme
    When I send a POST request to "/api/theme" with json:
      """
      {
        "name": "test",
        "content": {
          "pages": [
            {
              "id": "11111111-1111-1111-1111-111111111111",
              "name": "page name",
              "type": "replica",
              "lastActionId": "lastActionId",
              "master": {
                "id": "PebPageId",
                "lastActionId": "PebActionId",
                "idsMap": {
                  "master-id": "element-id"
                }
              },
              "data": {},
              "template": {
                "id": "page_template_id"
              },
              "stylesheets": {
                "styleid": {}
              },
              "context": {
                "contextId": {}
              },
              "variant": "front"
            }
          ],
          "context": { },
          "routing": [],
          "any": "fields",
          "canBe": "here"
        }
      }
      """
    Then print last response
    And the response status code should be 201
    And I store a response as "response"
    And response should contain json:
      """
      {
        "versions": [],
        "name": "test",
        "source": "*",
        "createdAt": "*",
        "updatedAt": "*",
        "id": "*"
      }
      """
      And model "Theme" with id "{{response.id}}" should contain json:
        """
        {
          "_id": "{{response.id}}"
        }
        """
      And model "ThemeSource" with id "{{response.source}}" should contain json:
        """
        {
          "_id": "{{response.source}}"
        }
        """
      And I look for model "ThemeSource" by following JSON and remember as "themeSource":
        """
        {
          "_id": "{{response.source}}"
        }
        """
      And model "ThemeSnapshot" with id "{{themeSource.snapshot}}" should contain json:
        """
        {
          "_id": "{{themeSource.snapshot}}"
        }
        """


  Scenario: Set theme image preview
    Given I use DB fixture "themes/theme-exists"
    When I send a PUT request to "/api/theme/{{themeId}}/image-preview" with json:
      """
      {
        "imagePreview": "test_image"
      }
      """
    Then print last response
    And the response status code should be 200
    And response should contain json:
      """
      {
         "_id": "{{themeId}}",
         "id": "{{themeId}}",
         "name": "Test theme",
         "picture": "test_image"
      }
      """
    And model "Theme" with id "{{themeId}}" should contain json:
      """
      {
        "picture": "test_image"
      }
      """

  Scenario: Set page image preview
    Given I use DB fixture "themes/theme-exists"
    When I send a PUT request to "/api/theme/{{themeId}}/page/{{pageId}}/image-preview" with json:
      """
      {
        "data": {
          "test": "data"
        }
      }
      """
    Then print last response
    And the response status code should be 200
    And response should contain json:
      """
      {
        "versions": [],
        "_id": "{{themeId}}",
        "name": "Test theme",
        "id": "{{themeId}}"
      }
      """
