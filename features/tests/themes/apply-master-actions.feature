Feature: Actions feature
  Background:
    And I remember as "themeId" following value:
      """
      "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
      """
    And I remember as "pageId" following value:
      """
      "11111111-1111-1111-1111-111111111111"
      """
    And I remember as "businessId" following value:
      """
      "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb"
      """
    And I remember as "sourceId" following value:
      """
      "cccccccc-cccc-cccc-cccc-cccccccccccc"
      """
    And I remember as "snapshotId" following value:
      """
      "ffffffff-ffff-ffff-ffff-ffffffffffff"
      """
    And I remember as "firstActionId" following value:
      """
      "first-action"
      """
    And I remember as "secondActionId" following value:
      """
      "second-action"
      """
    And I remember as "forkPageId" following value:
      """
      "22222222-2222-2222-2222-222222222222"
      """
    And I remember as "anotherForkPageId" following value:
      """
      "33333333-3333-3333-3333-3333333333"
      """
    And I remember as "nonForkPageId" following value:
      """
      "44444444-4444-4444-4444-4444444444"
      """
    Given I authenticate as a user with the following data:
      """
      {
        "email": "email@email.com",
        "roles": [
          {
            "name": "merchant",
            "permissions": [
              {
                "businessId": "{{businessId}}",
                "acls": []
              }
            ]
          }
        ]
      }
      """

  Scenario: Replace init actions for fork pages
    Given I use DB fixture "themes/multiple-pages"
    When I send a PUT request to "/api/theme/{{themeId}}/actions/apply" with json:
    """
    [
      {
        "id": "newAction",
        "targetPageId": "{{pageId}}",
        "affectedPageIds": [
          "{{pageId}}"
        ],
        "createdAt": "2020-08-18T11:12:11.377Z",
        "effects": [
          {
            "type": "template:init",
            "target": "shop",
            "payload": {
              "id": "333",
              "children": []
            }
          }
        ]
      },
      {
        "id": "new-fork-action",
        "targetPageId": "{{forkPageId}}",
        "affectedPageIds": [
          "{{forkPageId}}"
        ],
        "createdAt": "2020-08-18T11:12:11.377Z",
        "effects": [
          {
            "type": "page:init",
            "target": "pages:{{forkPageId}}",
            "payload": {
              "id": "{{forkPageId}}",
              "variant": "front",
              "type": "replica",
              "master": {
                "id": "{{pageId}}",
                "lastActionId": "{{secondActionId}}",
                "idsMap": {}
              },
              "templateId": "1efed217-c12c-4988-8d3a-92ec5f267d97",
              "stylesheetIds": {
                "desktop": "1e0203d4-c6d1-48cd-98ec-4cf7d1be9dc2",
                "tablet": "17bcb55b-f788-4380-9ab4-5db71c1d3022",
                "mobile": "15ce8ca0-9f04-4019-abc6-d85349a122b3"
              },
              "contextId": "10b8194d-d72b-4427-86f4-920552cea9ea"
            }
          }
        ]
      }
    ]
    """
    Then print last response
    And the response status code should be 200
        And the response should contain json:
      """
      {
        "id": "{{snapshotId}}"
      }
      """
    And model "ThemeSource" with id "{{sourceId}}" should contain json:
      """
      {
        "actions": [
          {
            "id": "newAction",
            "effects": [
              {
                "payload": {
                  "id": "333"
                }
              }
            ]
          },
          {
            "id": "new-fork-action",
            "effects": [
              {
                "payload": {
                  "master": {
                    "lastActionId": "{{secondActionId}}"
                  }
                }
              }
            ]
          }
        ]
      }
      """
