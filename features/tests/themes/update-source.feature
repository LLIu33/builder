Feature: Update source
  Background:
    And I remember as "themeId" following value:
      """
      "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
      """
    And I remember as "sourceId" following value:
      """
      "cccccccc-cccc-cccc-cccc-cccccccccccc"
      """
    And I remember as "businessId" following value:
      """
      "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb"
      """
    Given I authenticate as a user with the following data:
      """
      {
        "email": "email@email.com",
        "roles": [
          {
            "name": "merchant",
            "permissions": [
              {
                "businessId": "{{businessId}}",
                "acls": []
              }
            ]
          }
        ]
      }
      """

  Scenario: Set source previews
    Given I use DB fixture "themes/theme-exists"
    When I send a PATCH request to "/api/theme/{{themeId}}/source/{{sourceId}}/previews" with json:
      """
      {
        "data": "test_data"
      }
      """
    Then print last response
    And the response status code should be 200
    And response should contain json:
      """
      {
        "data": "test_data"
      }
      """
    And model "ThemeSource" with id "{{sourceId}}" should contain json:
      """
      {
        "previews": {
          "data": "test_data"
        }
      }
      """
