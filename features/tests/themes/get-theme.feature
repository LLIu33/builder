Feature: Get theme
  Background:
    And I remember as "themeId" following value:
      """
      "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
      """
    And I remember as "pageId" following value:
      """
      "11111111-1111-1111-1111-111111111111"
      """
    And I remember as "businessId" following value:
      """
      "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb"
      """
    Given I authenticate as a user with the following data:
      """
      {
        "email": "email@email.com",
        "roles": [
          {
            "name": "merchant",
            "permissions": [
              {
                "businessId": "{{businessId}}",
                "acls": []
              }
            ]
          }
        ]
      }
      """

  Scenario: Get theme
    Given I use DB fixture "themes/theme-exists"
    When I send a GET request to "/api/theme/{{themeId}}"
    Then print last response
    And the response status code should be 200
    And the response should contain json:
      """
      {
        "_id": "{{themeId}}",
        "name": "Test theme",
        "source": {
          "_id": "cccccccc-cccc-cccc-cccc-cccccccccccc",
          "snapshot": "*"
        }
      }
      """
 