Feature: Actions feature
  Background:
    And I remember as "themeId" following value:
      """
      "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
      """
    And I remember as "pageId" following value:
      """
      "11111111-1111-1111-1111-111111111111"
      """
    And I remember as "businessId" following value:
      """
      "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb"
      """
    And I remember as "sourceId" following value:
      """
      "cccccccc-cccc-cccc-cccc-cccccccccccc"
      """
    And I remember as "firstActionId" following value:
      """
      "first-action"
      """
    And I remember as "actionId" following value:
      """
      "test-action"
      """
    And I remember as "newActionId" following value:
      """
      "22222222-2222-2222-2222-222222222222"
      """
    And I remember as "snapshotId" following value:
      """
      "ffffffff-ffff-ffff-ffff-ffffffffffff"
      """ 
    And I remember as "snapshotHash" following value:
      """
      "4baf9221"
      """ 
    Given I authenticate as a user with the following data:
      """
      {
        "email": "email@email.com",
        "roles": [
          {
            "name": "merchant",
            "permissions": [
              {
                "businessId": "{{businessId}}",
                "acls": []
              }
            ]
          }
        ]
      }
      """

  Scenario: Add page action
    Given I use DB fixture "themes/theme-exists"
    When I send a POST request to "/api/theme/{{themeId}}/action" with json:
      """
      {
        "id": "{{newActionId}}",
        "createdAt": "2020-09-22T11:22:46.333Z",
        "targetPageId": "{{pageId}}",
        "affectedPageIds": [
          "{{pageId}}"
        ],
        "effects": [
          {
            "type": "template:append-element",
            "target": "templates:cedcd983-ed50-4eb6-81c8-3d3a5cdc43c9",
            "payload": {
              "to": "12e89f53-0165-4d9f-b0be-726ba848bbd6",
              "element": {
                "id": "3e53aa0c-8818-4bbe-be45-315af9aeb951",
                "type": "text",
                "data": {
                  "text": "Text",
                  "sync": false
                },
                "children": []
              }
            }
          },
          {
            "type": "stylesheet:update",
            "target": "stylesheets:2e0af977-3ab4-4441-bf82-1e1a1adcd39e",
            "payload": {
              "3e53aa0c-8818-4bbe-be45-315af9aeb951": {
                "width": 32,
                "height": 18,
                "fontSize": 15,
                "fontWeight": "bold",
                "margin": "0 0 0 0"
              }
            }
          },
          {
            "type": "stylesheet:update",
            "target": "stylesheets:d33f0564-5e8d-4257-a74b-8bbfc6c6ba18",
            "payload": {
              "3e53aa0c-8818-4bbe-be45-315af9aeb951": {
                "width": 32,
                "height": 18,
                "fontSize": 15,
                "fontWeight": "bold",
                "margin": "0 0 0 0"
              }
            }
          },
          {
            "type": "stylesheet:update",
            "target": "stylesheets:25d0ebb6-7c76-4131-9078-ce3c655d231d",
            "payload": {
              "3e53aa0c-8818-4bbe-be45-315af9aeb951": {
                "width": 32,
                "height": 18,
                "fontSize": 15,
                "fontWeight": "bold",
                "margin": "0 0 0 0"
              }
            }
          },
          {
            "type": "context-schema:update",
            "target": "contextSchemas:7072c359-e8b2-407f-85cb-56c9530c0d0d",
            "payload": null
          }
        ]
      }
      """
    Then print last response
    And the response status code should be 201
    And model "ThemeSource" with id "{{sourceId}}" should contain json:
      """
      {
        "actions": [
          {
            "id": "{{actionId}}",
            "targetPageId": "{{pageId}}",
            "affectedPageIds": [
              "{{pageId}}"
            ]
          },
          {
            "id": "{{newActionId}}",
            "targetPageId": "{{pageId}}",
            "affectedPageIds": [
              "{{pageId}}"
            ]
          }
        ]
      }
      """
    And model "ThemeSnapshot" with id "{{snapshotId}}" should not contain json:
      """
      {
        "hash": "{{snapshotHash}}"
      }
      """

  Scenario: Add page action
    Given I use DB fixture "themes/theme-exists"
    When I send a POST request to "/api/theme/{{themeId}}/action" with json:
      """
      {
        "id": "{{newActionId}}",
        "targetPageId": "{{pageId}}",
        "affectedPageIds": [
          "{{pageId}}"
        ],
        "effects": [
          {
            "type": "template:append-element",
            "target": "templates:11111",
            "payload": {
              "to": "22222",
              "element": {
                "id": "33333",
                "type": "text",
                "data": {
                  "text": "Text",
                  "sync": false
                },
                "children": []
              }
            }
          }
        ]
      }
      """
    Then print last response
    And the response status code should be 400
    And the response should contain json:
    """
    {
      "message": "Validation failed",
      "errors": "Wrong request: for action with the id {{newActionId}}"
    }
    """

  Scenario: Delete page action
    Given I use DB fixture "themes/theme-exists"
    When I send a DELETE request to "/api/theme/{{themeId}}/action/{{actionId}}"
    Then print last response
    And the response status code should be 200
    And model "ThemeSource" with id "{{sourceId}}" should not contain json:
      """
      {
        "actions": [
          {
            "id": "{{actionId}}"
          }
        ]
      }
      """
    And print RabbitMQ message list
    And RabbitMQ exchange "async_events" should contain following ordered messages:
      """
      [
        {
          "name": "builder-test.builder.event.theme.update-snapshot",
          "payload": {
            "theme": {
              "id": "{{themeId}}"
            }
          }
        }
      ]
      """

  Scenario: Get page actions
    Given I use DB fixture "themes/theme-exists"
    When I send a GET request to "/api/theme/{{themeId}}/actions?limit=1&offset=0"
    Then print last response
    And the response status code should be 200
    And the response should contain json:
      """
      [
        {
          "id": "{{actionId}}"
        }
      ]
      """
    And the response should not contain json:
      """
      [
        {
          "id": "{{firstActionId}}"
        }
      ]
      """
