Feature: Update theme
  Background:
    And I remember as "themeId" following value:
      """
      "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
      """
    Given I authenticate as a user with the following data:
      """
      {
        "email": "email@email.com",
        "roles": [
          {
            "name": "merchant",
            "permissions": [
              {
                "businessId": "{{businessId}}",
                "acls": []
              }
            ]
          }
        ]
      }
      """

  Scenario: Update theme name
    Given I use DB fixture "themes/theme-exists"
    When I send a PATCH request to "/api/theme/{{themeId}}/name" with json:
      """
      {
        "name": "New name"
      }
      """
    Then print last response
    And the response status code should be 200
    And response should contain json:
      """
      {
         "_id": "{{themeId}}",
         "id": "{{themeId}}",
         "name": "New name"
      }
      """
    And model "Theme" with id "{{themeId}}" should contain json:
      """
      {
        "name": "New name"
      }
      """
    And model "Theme" with id "{{themeId}}" should not contain json:
      """
      {
        "name": "Test theme"
      }
      """
