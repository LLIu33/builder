Feature: Update snapshot
  Background:
    And I remember as "themeId" following value:
      """
      "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
      """
    And I remember as "snapshotId" following value:
      """
      "ffffffff-ffff-ffff-ffff-ffffffffffff"
      """

  Scenario: Received message for snapshot update
    Given I use DB fixture "themes/theme-exists"
    When I publish in RabbitMQ channel "async_events_test_micro" message with json:
      """
      {
        "name": "builder-test.builder.event.theme.update-snapshot",
        "payload": {
          "theme": {
            "id": "{{themeId}}"
          }
        }
      }

      """
    And look for model "ThemeSnapshot" by following JSON and remember as "existingSnapshot":
      """
      {
        "_id": "{{snapshotId}}"
      }
      """
    Then I process messages from RabbitMQ "async_events_test_micro" channel
    And print RabbitMQ message list
    And model "ThemeSnapshot" with id "{{snapshotId}}" should not contain json:
      """
      {
        "updatedAt": "{{existingSnapshot.updatedAt}}"
      }
      """

  Scenario: Received message for snapshot update for already compiled snapshot
    Given I use DB fixture "themes/snapshot-already-compiled"
    When I publish in RabbitMQ channel "async_events_test_micro" message with json:
      """
      {
        "name": "builder-test.builder.event.theme.update-snapshot",
        "payload": {
          "theme": {
            "id": "{{themeId}}"
          }
        }
      }

      """
    And look for model "ThemeSnapshot" by following JSON and remember as "existingSnapshot":
      """
      {
        "_id": "{{snapshotId}}"
      }
      """
    Then I process messages from RabbitMQ "async_events_test_micro" channel
    And print RabbitMQ message list
    And model "ThemeSnapshot" with id "{{snapshotId}}" should contain json:
      """
      {
        "updatedAt": "{{existingSnapshot.updatedAt}}"
      }
      """
