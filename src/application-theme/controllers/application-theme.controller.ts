import { ConflictException, Controller, Delete, NotFoundException, Post, Put, UseGuards } from '@nestjs/common';
import { AbstractController, JwtAuthGuard, ParamModel, Roles, RolesEnum, User, UserTokenInterface } from '@pe/nest-kit';
import { BusinessModel } from '../../common/interfaces/entities';
import { ThemeModel, ThemeService, ThemesSchemaNamesEnum } from '../../themes';
import { ApplicationModel } from '../decorators/application-model';
import { ApplicationInterface } from '../interfaces';
import { ApplicationThemeService, ThemeInstallerService } from '../services';
import { ApplicationThemeModel } from '../interfaces/entities';
import { AppThemeCreateVoter, AppThemeDeleteVoter } from '../voters';
import { BusinessSchemaNamesEnum } from '../../common/enums';

@Controller('/business/:businessId/application/:applicationId/theme/:themeId')
@UseGuards(JwtAuthGuard)
@Roles(RolesEnum.merchant)
export class ApplicationThemeController extends AbstractController {
  constructor(
    private readonly themeInstaller: ThemeInstallerService,
    private readonly appThemeService: ApplicationThemeService,
    private readonly themeService: ThemeService,
  ) {
    super();
  }

  @Post('/install')
  public async installThemeFromTemplate(
    @ParamModel(':businessId', BusinessSchemaNamesEnum.Business, true) business: BusinessModel,
    @ApplicationModel('applicationId') application: ApplicationInterface,
    @ParamModel(':themeId', ThemesSchemaNamesEnum.Theme) templateTheme: ThemeModel,
    @User() user: UserTokenInterface,
  ): Promise<ThemeModel> {

    await this.denyAccessUnlessGranted(AppThemeCreateVoter.CREATE, application, user);

    await templateTheme
    .populate({
      path: 'source',
      populate: {
        path: 'snapshot',
      },
    })
    .execPopulate();

    const installedTheme: ThemeModel = await this.themeInstaller.install(application, templateTheme);
    await this.appThemeService.activateThemeForApplication(application, installedTheme);

    return this.themeService.findById(installedTheme.id);
  }

  @Put('/switch')
  public async switchThemeForApp(
    @ParamModel(':businessId', BusinessSchemaNamesEnum.Business, true) business: BusinessModel,
    @ApplicationModel('applicationId') application: ApplicationInterface,
    @ParamModel(':themeId', ThemesSchemaNamesEnum.Theme) theme: ThemeModel,
    @User() user: UserTokenInterface,
  ): Promise<ThemeModel> {

    await this.denyAccessUnlessGranted(AppThemeCreateVoter.CREATE, application, user);

    const existingAppTheme: ApplicationThemeModel =
      await this.appThemeService.findByThemeAndApplication(theme, application);
    if (!existingAppTheme) {
      throw new NotFoundException(`There is no theme "${theme.id}" for application "${application.id}".`);
    }

    await this.appThemeService.activateThemeForApplication(application, theme);

    return this.themeService.findById(theme.id);
  }

  @Post('/duplicate')
  public async duplicateInstalledTheme(
    @ParamModel(':businessId', BusinessSchemaNamesEnum.Business, true) business: BusinessModel,
    @ApplicationModel('applicationId') application: ApplicationInterface,
    @ParamModel(':themeId', ThemesSchemaNamesEnum.Theme) templateTheme: ThemeModel,
    @User() user: UserTokenInterface,
  ): Promise<ThemeModel> {

    await this.denyAccessUnlessGranted(AppThemeCreateVoter.CREATE, application, user);

    const existingAppTheme: ApplicationThemeModel =
      await this.appThemeService.findByThemeAndApplication(templateTheme, application);
    if (!existingAppTheme) {
      throw new NotFoundException(`There is no theme "${templateTheme.id}" for application "${application.id}".`);
    }

    await templateTheme
    .populate({
      path: 'source',
      populate: {
        path: 'snapshot',
      },
    })
    .execPopulate();

    const installedTheme: ThemeModel = await this.themeInstaller.install(application, templateTheme);

    return this.themeService.findById(installedTheme.id);
  }

  @Delete()
  public async removeInstalledTheme(
    @ParamModel(':businessId', BusinessSchemaNamesEnum.Business, true) business: BusinessModel,
    @ApplicationModel('applicationId') application: ApplicationInterface,
    @ParamModel(':themeId', ThemesSchemaNamesEnum.Theme) theme: ThemeModel,
    @User() user: UserTokenInterface,
  ): Promise<any> {

    await this.denyAccessUnlessGranted(AppThemeDeleteVoter.DELETE, application, user);

    const appTheme: ApplicationThemeModel =
      await this.appThemeService.findByThemeAndApplication(theme, application);
    if (!appTheme) {
      throw new NotFoundException(`There is no theme "${theme.id}" for application "${application.id}".`);
    }
    if (appTheme && appTheme.isActive) {
      throw new ConflictException(
        `Theme with id "${theme.id}" is active for application "${application.id}" and can not be removed.`,
      );
    }

    await this.appThemeService.remove(appTheme);

    return { status: 'ok' };
  }
}
