import { Controller, Get, Query, UseGuards } from '@nestjs/common';
import { ApiUseTags } from '@nestjs/swagger';
import { AbstractController, JwtAuthGuard, ParamModel, Roles, RolesEnum, User, UserTokenInterface } from '@pe/nest-kit';
import { BusinessSchemaNamesEnum } from '../../common/enums';
import { BusinessModel } from '../../common/interfaces/entities';
import { ApplicationModel } from '../decorators/application-model';
import { ApplicationInterface } from '../interfaces';
import { ApplicationThemeModel } from '../interfaces/entities';
import { ApplicationThemeService, CompiledThemeService } from '../services';
import { AppThemeCreateVoter } from '../voters';

@Controller('/business/:businessId/application/:applicationId')
@ApiUseTags('application')
@UseGuards(JwtAuthGuard)
@Roles(RolesEnum.merchant)
export class ApplicationController extends AbstractController {
  constructor(
    private readonly applicationThemeService: ApplicationThemeService,
    private readonly compiledThemeService: CompiledThemeService,
  ) {
    super();
  }

  @Get('/themes')
  public async getAllApplicationThemes(
    @ParamModel(':businessId', BusinessSchemaNamesEnum.Business, true) business: BusinessModel,
    @ApplicationModel('applicationId') application: ApplicationInterface,
    @User() user: UserTokenInterface,
  ): Promise<ApplicationThemeModel[]> {
    await this.denyAccessUnlessGranted(AppThemeCreateVoter.CREATE, application, user);

    return this.applicationThemeService.findByApplication(application);
  }

  @Get('/themes/active')
  public async getActiveTheme(
    @ParamModel(':businessId', BusinessSchemaNamesEnum.Business, true) business: BusinessModel,
    @ApplicationModel('applicationId') application: ApplicationInterface,
    @User() user: UserTokenInterface,
  ): Promise<ApplicationThemeModel> {
    await this.denyAccessUnlessGranted(AppThemeCreateVoter.CREATE, application, user);

    return this.applicationThemeService.findActiveByApplication(application);
  }

  @Get('/preview')
  public async getShopPreview(
    @ParamModel(':businessId', BusinessSchemaNamesEnum.Business, true) business: BusinessModel,
    @ApplicationModel('applicationId') application: ApplicationInterface,
    @Query('include') queryInclude: string,
    @Query('page') pageVariant: string,
    @User() user: UserTokenInterface,
  ): Promise<any> {
    await this.denyAccessUnlessGranted(AppThemeCreateVoter.CREATE, application, user);

    const result: any = {
      current: null,
      published: null,
    };

    const include: string[] = (queryInclude || 'current,published').toLowerCase().split(',');

    if (include.includes('current')) {
      const appTheme: ApplicationThemeModel = await this.applicationThemeService.findActiveByApplication(application);
      if (appTheme) {
        await appTheme
        .populate({
          path: 'theme',
          populate: {
            path: 'source',
            populate: {
              path: 'snapshot',
            },
          },
        })
        .execPopulate();

        result.current = appTheme.theme.source.snapshot;
      }
    }

    if (include.includes('published')) {
      result.published = await this.compiledThemeService.findByApplicationWithPages(application, pageVariant);
    }

    return result;
  }
}
