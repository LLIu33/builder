import { Controller, Put, UseGuards } from '@nestjs/common';
import { AbstractController, JwtAuthGuard, ParamModel, Roles, RolesEnum } from '@pe/nest-kit';
import { BusinessSchemaNamesEnum } from '../../common/enums';
import { BusinessModel } from '../../common/interfaces/entities';
import { ThemeModel, ThemesSchemaNamesEnum } from '../../themes';
import { ApplicationModel } from '../decorators/application-model';
import { ApplicationInterface } from '../interfaces';
import { InstantThemeInstallerService } from '../services';

@Controller('business/:businessId/application/:applicationId')
@UseGuards(JwtAuthGuard)
@Roles(RolesEnum.merchant)
export class InstantSetupController extends AbstractController {
  constructor(
    private readonly instantThemeInstaller: InstantThemeInstallerService,
  ) {
    super();
  }

  @Put('/template/:templateId/instant-setup')
  public async installThemeWithPublishing(
    @ParamModel(':businessId', BusinessSchemaNamesEnum.Business, true) business: BusinessModel,
    @ApplicationModel('applicationId') application: ApplicationInterface,
    @ParamModel(':templateId', ThemesSchemaNamesEnum.Theme) templateTheme: ThemeModel,
  ): Promise<any> {

    await templateTheme
    .populate({
      path: 'source',
      populate: {
        path: 'snapshot',
      },
    })
    .execPopulate();

    return this.instantThemeInstaller.install(application, templateTheme);
  }
}
