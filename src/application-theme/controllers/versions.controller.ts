import { Body, Controller, Delete, Get, NotAcceptableException, Patch, Post, Put } from '@nestjs/common';
import { ParamModel } from '@pe/nest-kit';
import {
  ThemeModel,
  ThemeSnapshotModel,
  ThemesSchemaNamesEnum,
  ThemeVersionCreateDto,
  ThemeVersionModel,
  ThemeVersionService,
} from '../../themes';
import { ApplicationThemeModel } from '../interfaces/entities';
import { ApplicationThemeService, ThemePublisherService } from '../services';


const VERSION_KEY: string = ':versionId';
const THEME_KEY: string = ':themeId';

@Controller('/theme/:themeId')
export class VersionsController {
  constructor(
    private readonly appThemeService: ApplicationThemeService,
    private readonly versionService: ThemeVersionService,
    private readonly themePublisher: ThemePublisherService,
  ) { }

  @Get('/versions')
  public async getThemeVersionsList(
    @ParamModel(THEME_KEY, ThemesSchemaNamesEnum.Theme) theme: ThemeModel,
  ): Promise<any> {
    await theme.populate('versions').execPopulate();

    return theme.versions;
  }

  @Post('/version')
  public async createThemeVersion(
    @ParamModel(THEME_KEY, ThemesSchemaNamesEnum.Theme) theme: ThemeModel,
    @Body() body: ThemeVersionCreateDto,
  ): Promise<ThemeVersionModel> {
    return this.versionService.create(theme, body);
  }

  @Delete('version/:versionId')
  public async deleteThemeVersion(
    @ParamModel(THEME_KEY, ThemesSchemaNamesEnum.Theme) theme: ThemeModel,
    @ParamModel(VERSION_KEY, ThemesSchemaNamesEnum.ThemeVersion) version: ThemeVersionModel,
  ): Promise<any> {
    await this.versionService.remove(version, theme);

    return { status: 'ok' };
  }

  @Put('version/:versionId/publish')
  public async publishShopThemeVersion(
    @ParamModel(THEME_KEY, ThemesSchemaNamesEnum.Theme) theme: ThemeModel,
    @ParamModel(VERSION_KEY, ThemesSchemaNamesEnum.ThemeVersion) version: ThemeVersionModel,
  ): Promise<ThemeVersionModel> {
    const appTheme: ApplicationThemeModel = await this.appThemeService.findByThemeWithApplication(theme);
    if (!appTheme) {
      throw new NotAcceptableException(
        `Theme "${theme.id}" can't be published. It is not tied to any application`,
      );
    }

    await version.populate({
      path: 'source',
      populate: {
        path: 'snapshot',
      },
    })
    .populate('theme')
    .execPopulate();

    await this.themePublisher.publish(appTheme.application, appTheme, version);

    return this.versionService.findById(version.id);
  }

  @Put('version/:versionId/restore')
  public async restoreThemeVersion(
    @ParamModel(THEME_KEY, ThemesSchemaNamesEnum.Theme) theme: ThemeModel,
    @ParamModel(VERSION_KEY, ThemesSchemaNamesEnum.ThemeVersion) version: ThemeVersionModel,
  ): Promise<any> {
    const appTheme: ApplicationThemeModel = await this.appThemeService.findByThemeWithApplication(theme);
    if (!appTheme) {
      throw new NotAcceptableException(
        `Theme "${theme.id}" can't be published. It is not tied to any application`,
      );
    }

    await version.populate({
      path: 'source',
      populate: {
        path: 'snapshot',
      },
    })
    .execPopulate();

    await theme.populate({
      path: 'source',
      populate: {
        path: 'snapshot',
      },
    })
    .execPopulate();

    await this.versionService.restore(version, theme);
    await this.versionService.setActiveVersion(version);

    return this.versionService.findById(version.id);
  }

  @Get('version/active')
  public async getActiveThemeVersion(
    @ParamModel(THEME_KEY, ThemesSchemaNamesEnum.Theme) theme: ThemeModel,
  ): Promise<any> {
    return this.versionService.getActiveVersion(theme);
  }

  @Get('version/:versionId')
  public async getPublishShopThemeVersion(
    @ParamModel(THEME_KEY, ThemesSchemaNamesEnum.Theme) theme: ThemeModel,
    @ParamModel(VERSION_KEY, ThemesSchemaNamesEnum.ThemeVersion) version: ThemeVersionModel,
  ): Promise<any> {
    await version.populate('source').execPopulate();
    const publishedVersion: ThemeVersionModel = await this.versionService.getPublishedVersion(theme);

    return {
      id: theme.id,
      name: theme.name,
      picture: theme.picture,
      publishedId: publishedVersion ? publishedVersion._id : null,
      source: version.source,
      sourceId: theme.source,
      versionsIds: theme.versions,
    };
  }

  @Patch('version/:versionId/active')
  public async setActiveThemeVersion(
    @ParamModel(VERSION_KEY, ThemesSchemaNamesEnum.ThemeVersion) version: ThemeVersionModel,
  ): Promise<any> {
    await this.versionService.setActiveVersion(version);

    return this.versionService.findById(version.id);
  }

  @Get('version/:versionId/snapshot')
  public async getActualSnapshot(
    @ParamModel(VERSION_KEY, ThemesSchemaNamesEnum.ThemeVersion) version: ThemeVersionModel,
  ): Promise<ThemeSnapshotModel> {
    await version.populate({
      path: 'source',
      populate: {
        path: 'snapshot',
      },
    })
    .execPopulate();

    return version.source.snapshot;
  }
}
