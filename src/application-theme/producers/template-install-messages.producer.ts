import { Injectable } from '@nestjs/common';
import { RabbitMqClient } from '@pe/nest-kit';
import { ThemeModel } from '../../themes';
import { TemplateInstallMessagesEnum } from '../enums';
import { ApplicationInterface } from '../interfaces';
import { ApplicationThemeModel } from '../interfaces/entities';

@Injectable()
export class TemplateInstallMessagesProducer {
  constructor(
    private readonly rabbitClient: RabbitMqClient,
  ) { }

  public async sendTempateInstalled(
    application: ApplicationInterface,
    appTheme: ApplicationThemeModel,
    installedTheme: ThemeModel,
  ): Promise<void> {
    const payload: any = {
      installedTheme: {
        id: installedTheme.id,
      },
    };

    await this.triggerEvent(TemplateInstallMessagesEnum.TemplateInstalled, payload);
  }

  private async triggerEvent(eventName: string, payload: any): Promise<void> {
    await this.rabbitClient.send(
      {
        channel: eventName,
        exchange: 'async_events',
      },
      {
        name: eventName,
        payload,
      },
    );
  }
}
