import { Injectable } from '@nestjs/common';
import { RabbitMqClient } from '@pe/nest-kit';
import { ApplicationPageModel, ApplicationThemeModel, CompiledThemeModel } from '../interfaces/entities';
import { CompiledThemeMessagesEnum } from '../enums';
import { ApplicationTypesEnum } from '../../common/enums';

@Injectable()
export class CompiledThemeMessagesProducer {
  constructor(
    private readonly rabbitClient: RabbitMqClient,
    private readonly applicationType: ApplicationTypesEnum,
  ) { }

  public async sendThemePublished(
    appTheme: ApplicationThemeModel,
    compiledTheme: CompiledThemeModel,
    pages: ApplicationPageModel[],
  ): Promise<void> {
    const payload: any = {
      application: {
        id: appTheme.application.id,
        type: this.applicationType,
      },
      compiled: {
        theme: compiledTheme,
        themePages: pages,
      },
      id: appTheme.theme.id,
      isDeployed: true,
      theme: {
        id: appTheme.theme.id,
      },
    };

    await this.triggerEvent(CompiledThemeMessagesEnum.ThemePublished, payload);
  }

  private async triggerEvent(eventName: string, payload: any): Promise<void> {
    await this.rabbitClient.send(
      {
        channel: eventName,
        exchange: 'async_events',
      },
      {
        name: eventName,
        payload,
      },
    );
  }
}
