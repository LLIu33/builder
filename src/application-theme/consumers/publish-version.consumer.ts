import { Controller, Logger } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import * as uuid from 'uuid';
import { ThemeModel, ThemeService, ThemeVersionModel, ThemeVersionService } from '../../themes';
import { InstallTemplateDto } from '../dto';
import { TemplateInstallMessagesEnum } from '../enums';
import { ApplicationInterface, ApplicationThemeModel } from '../interfaces';
import { ApplicationThemeService, ThemePublisherService } from '../services';

@Controller()
export class PublishVersionConsumer {
  constructor(
    private readonly themeService: ThemeService,
    private readonly appThemeService: ApplicationThemeService,
    private readonly versionService: ThemeVersionService,
    private readonly themePublisher: ThemePublisherService,
    private readonly logger: Logger,
  ) { }

  @MessagePattern({
    name: TemplateInstallMessagesEnum.TemplateInstalled,
  })
  public async onUpdateSnapshotRequest(dto: InstallTemplateDto): Promise<void> {
    const installedTheme: ThemeModel = await this.themeService.findById(dto.installedTheme.id);
    const appTheme: ApplicationThemeModel = await this.appThemeService.findByThemeWithApplication(installedTheme);
    const version: ThemeVersionModel = await this.versionService.create(
      installedTheme,
      { name: uuid.v4() },
    );
    await this.themePublisher.publish(appTheme.application, appTheme, version);
    this.logger.log(`Publish version ${version.id}`);
  }
}
