export enum ApplicationThemeSchemaNamesEnum {
  ApplicationPage = 'ApplicationPage',
  ApplicationTheme = 'ApplicationTheme',
  CompiledTheme = 'CompiledTheme',
}
