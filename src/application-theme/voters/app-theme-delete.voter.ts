import { Injectable } from '@nestjs/common';
import { AbstractVoter, AccessTokenPayload, AclActionsEnum, RolesEnum, Voter } from '@pe/nest-kit';
import { BusinessAccessValidator } from '@pe/nest-kit/modules/auth/access-validators/special';
import { ApplicationInterface } from '../interfaces';

@Injectable()
@Voter()
export class AppThemeDeleteVoter extends AbstractVoter {
  public static readonly DELETE: string = 'delete-app-theme';

  protected async supports(attribute: string, subject: any): Promise<boolean> {
    return attribute === AppThemeDeleteVoter.DELETE;
  }

  protected async voteOnAttribute(
    attribute: string,
    application: ApplicationInterface,
    user: AccessTokenPayload,
  ): Promise<boolean> {
    const isAccessAllowed: boolean = user.isAdmin() || BusinessAccessValidator.isAccessAllowed(
      user.getRole(RolesEnum.merchant),
      [
        { microservice: 'builder', action: AclActionsEnum.delete },
      ],
      application.business.id,
    );

    return user && application.business && isAccessAllowed;
  }
}
