import { Injectable } from '@nestjs/common';
import { AbstractVoter, AccessTokenPayload, AclActionsEnum, RolesEnum, UserRoleMerchant, Voter } from '@pe/nest-kit';

import { BusinessAccessValidator } from '@pe/nest-kit/modules/auth/access-validators/special';
import { ApplicationInterface } from '../interfaces';

@Injectable()
@Voter()
export class AppThemeCreateVoter extends AbstractVoter {
  public static readonly CREATE: string = 'create-app-theme';

  protected async supports(attribute: string, subject: any): Promise<boolean> {
    return attribute === AppThemeCreateVoter.CREATE;
  }

  protected async voteOnAttribute(
    attribute: string,
    application: ApplicationInterface,
    user: AccessTokenPayload,
  ): Promise<boolean> {
    const isAccessAllowed: boolean = user.isAdmin() || BusinessAccessValidator.isAccessAllowed(
      user.getRole(RolesEnum.merchant),
      [
        { microservice: 'builder', action: AclActionsEnum.create },
      ],
      application.business.id,
    );

    return user && application.business && isAccessAllowed;
  }
}
