import { Type } from 'class-transformer';
import { IsDefined, ValidateNested } from 'class-validator';
import { ThemeReferenceDto } from '../../themes';

export class InstallTemplateDto {
  @ValidateNested()
  @IsDefined()
  @Type(() => ThemeReferenceDto)
  public installedTheme: ThemeReferenceDto;
}
