import { RabbitMqClient } from '@pe/nest-kit';
import { ApplicationTypesEnum } from '../../common/enums';
import { CompiledThemeMessagesProducer } from '../producers';

export const getCompiledThemeMessagesProducerProvider: any = (applicationType: ApplicationTypesEnum) => {
  return {
    inject: [RabbitMqClient],
    provide: CompiledThemeMessagesProducer,
    useFactory: (rabbitMqClient: RabbitMqClient) => {
      return new CompiledThemeMessagesProducer(rabbitMqClient, applicationType);
    },
  };
};
