import { getModelToken } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ProvidersEnum } from '../../common/enums';
import { ApplicationInterface } from '../interfaces';

export const getApplicationModelProvider: any = (applicationSchemaName: string) => {
  return {
    inject: [getModelToken(applicationSchemaName)],
    provide: ProvidersEnum.ApplicationModel,
    useFactory: (appModel: Model<ApplicationInterface>) => {
      return appModel;
    },
  };
};
