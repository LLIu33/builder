import { ApplicationInterface } from '../application.interface';
import { ThemeInterface } from '../../../themes';

export interface ApplicationThemeInterface {
  isActive: boolean;
  isDeployed: boolean;
  application: ApplicationInterface;
  theme: ThemeInterface;
}
