import { ApplicationThemeInterface } from './application-theme.interface';
import { Document } from 'mongoose';
import { ThemeModel } from '../../../themes';

export interface ApplicationThemeModel extends ApplicationThemeInterface, Document {
  theme: ThemeModel;
}
