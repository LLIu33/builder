import { PebContextSchema, PebShopData, PebShopRoute } from '@pe/builder-core';
import { ApplicationInterface } from '../';
import { ApplicationPageInterface } from './application-page.interface';

export interface CompiledThemeInterface {
  data: PebShopData;
  routing: PebShopRoute[];
  context: PebContextSchema;
  application: ApplicationInterface;
  pages: ApplicationPageInterface[];
}
