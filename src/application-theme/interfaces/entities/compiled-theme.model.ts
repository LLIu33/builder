import { CompiledThemeInterface } from './compiled-theme.interface';
import { Document } from 'mongoose';
import { ApplicationPageModel } from './application-page.model';

export interface CompiledThemeModel extends CompiledThemeInterface, Document {
  pages: ApplicationPageModel[];
}
