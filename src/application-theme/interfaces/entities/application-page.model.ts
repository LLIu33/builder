import { Document } from 'mongoose';
import { ApplicationPageInterface } from './application-page.interface';

export interface ApplicationPageModel extends ApplicationPageInterface, Document { }
