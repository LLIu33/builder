import {
  PebActionId,
  PebContextSchema,
  PebPageId,
  PebPageType,
  PebPageVariant,
  PebStylesheet,
  PebTemplate,
} from '@pe/builder-core';
import { ApplicationInterface } from '../';

export interface ApplicationPageInterface {
  name: string;
  variant: PebPageVariant;
  type: PebPageType;
  master: null | {
    id: PebPageId;
    lastActionId: PebActionId;
  };
  lastActionId: string;
  data: {
    url?: string;
    mark?: string;
    preview?: string;
  };
  template: PebTemplate;
  stylesheets: {
    [screen: string]: PebStylesheet;
  };
  context: PebContextSchema;
  application: ApplicationInterface;
}
