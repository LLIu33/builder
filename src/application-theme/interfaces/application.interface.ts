import { Document } from 'mongoose';

export interface ApplicationInterface extends Document{
  id: string;
  business: { id: string };
}
