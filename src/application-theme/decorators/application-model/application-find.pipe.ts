import { ArgumentMetadata, Injectable, NotFoundException, PipeTransform } from '@nestjs/common';
import { Document } from 'mongoose';
import { OptionsInterface } from './options.interface';
import { ApplicationService } from '../../services';

@Injectable()
export class ApplicationFindPipe implements PipeTransform {
  constructor(
    private readonly applicationService: ApplicationService,
  ) { }

  public async transform(
    options: OptionsInterface,
    metadata: ArgumentMetadata,
  ): Promise<Document> {
    const id: string = options[metadata.data];

    const model: any = await this.applicationService.findById(id);

    if (!model) {
      throw new NotFoundException(
        `Application "${id}" not found!`,
      );
    }

    return model;
  }
}
