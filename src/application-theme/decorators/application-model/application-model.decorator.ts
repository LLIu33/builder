import { ParamData, PipeTransform, RouteParamMetadata } from '@nestjs/common';
import { CUSTOM_ROUTE_AGRS_METADATA, ROUTE_ARGS_METADATA } from '@nestjs/common/constants';
import { RouteParamtypes } from '@nestjs/common/enums/route-paramtypes.enum';
import { CustomParamFactory, Type } from '@nestjs/common/interfaces';
import { ApplicationFindPipe } from './application-find.pipe';

export function ApplicationModel(placeholder: string | object): any {
  return createPipesRouteParamDecorator(RouteParamtypes.PARAM)(placeholder);
}

const ParametersExtractor: any = (data: string | object, req: any) => {
  return req.params;
};

const createPipesRouteParamDecorator: any = (paramtype: RouteParamtypes) => {
  return (placeholder: string | object): ParameterDecorator => {
    return (target: any, key: any, index: any) => {
      const args: any = Reflect.getMetadata(ROUTE_ARGS_METADATA, target.constructor, key) || { };
      const pipes: Array<(Type<PipeTransform> | PipeTransform)> = [];

      pipes.push(ApplicationFindPipe);
      Reflect.defineMetadata(
        ROUTE_ARGS_METADATA,
        assignMetadata(args, paramtype, index, ParametersExtractor, placeholder, ...pipes),
        target.constructor,
        key,
      );
    };
  };
};

const assignMetadata: any = (
  args: RouteParamMetadata,
  paramtype: RouteParamtypes,
  index: number,
  factory: CustomParamFactory,
  data?: ParamData,
  ...pipes: Array<(Type<PipeTransform> | PipeTransform)>) => ({
  ...args,
  [`${paramtype}${CUSTOM_ROUTE_AGRS_METADATA}:${index}`]: {
    data,
    factory,
    index,
    pipes,
  },
});
