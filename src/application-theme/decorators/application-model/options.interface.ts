export interface OptionsInterface {
  placeholder: string;
  model: string;
  strict: boolean;
}
