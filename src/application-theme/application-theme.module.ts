import { Global, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ModuleOptionsInterface } from '../common/interfaces';
import { ThemesModule } from '../themes';
import { PublishVersionConsumer } from './consumers';
import {
  ApplicationController,
  ApplicationThemeController,
  InstantSetupController,
  VersionsController,
} from './controllers';
import { ApplicationFindPipe } from './decorators/application-model';
import { ApplicationThemeSchemaNamesEnum } from './enums';
import { TemplateInstallMessagesProducer } from './producers';
import { getApplicationModelProvider, getCompiledThemeMessagesProducerProvider } from './providers';
import {
  ApplicationPageSchemaFactory,
  ApplicationThemeSchemaFactory,
  CompiledThemeSchemaFactory,
} from './schemas';
import {
  ApplicationService,
  ApplicationThemeService,
  CompiledThemeService,
  InstantThemeInstallerService,
  ThemeInstallerService,
  ThemePublisherService,
} from './services';
import { AppThemeCreateVoter, AppThemeDeleteVoter } from './voters';

@Module({
  controllers: [ ],
  exports: [ ],
  providers: [ ],
})
@Global()
export class ApplicationThemeModule {
  public static forRoot(config: ModuleOptionsInterface): any {
    return {
      controllers: [
        ApplicationController,
        ApplicationThemeController,
        InstantSetupController,
        VersionsController,
        PublishVersionConsumer,
      ],
      imports: [
        MongooseModule.forFeature([
          {
            name: ApplicationThemeSchemaNamesEnum.ApplicationTheme,
            schema: ApplicationThemeSchemaFactory.create(config.applicationSchemaName),
          },
          {
            name: config.applicationSchemaName,
            schema: config.applicationSchema,
          },
          {
            name: ApplicationThemeSchemaNamesEnum.CompiledTheme,
            schema: CompiledThemeSchemaFactory.create(config.applicationSchemaName),
          },
          {
            name: ApplicationThemeSchemaNamesEnum.ApplicationPage,
            schema: ApplicationPageSchemaFactory.create(config.applicationSchemaName),
          },
        ]),
        ThemesModule,
      ],
      module: ApplicationThemeModule,
      providers: [
        ApplicationService,
        ApplicationThemeService,
        CompiledThemeService,
        getApplicationModelProvider(config.applicationSchemaName),
        getCompiledThemeMessagesProducerProvider(config.applicationType),
        InstantThemeInstallerService,
        ThemeInstallerService,
        ThemePublisherService,
        ApplicationFindPipe,
        AppThemeCreateVoter,
        AppThemeDeleteVoter,
        TemplateInstallMessagesProducer,
      ],
    };
  }
}
