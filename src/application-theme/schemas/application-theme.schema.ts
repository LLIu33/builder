import { Schema, VirtualType } from 'mongoose';
import { v4 as uuid } from 'uuid';
import { ThemesSchemaNamesEnum } from '../../themes';

export class ApplicationThemeSchemaFactory {
  private static schema: Schema;
  public static create(applicationSchema: string): Schema {
    if (!this.schema) {
      this.schema = new Schema(
        {
          _id: { type: String, default: uuid },
          application: { type: Schema.Types.String, required: true, ref: applicationSchema },
          isActive: Boolean,
          isDeployed: Boolean,
          theme: { type: Schema.Types.String, required: true, ref: ThemesSchemaNamesEnum.Theme },
        },
        {
          timestamps: { },
          toJSON: { virtuals: true },
        },
      );
      this.schema.virtual('id').get(function (): VirtualType {
        return this._id;
      });
    }

    return this.schema;
  }
}
