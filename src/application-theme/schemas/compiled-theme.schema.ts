import { Schema, VirtualType } from 'mongoose';
import { v4 as uuid } from 'uuid';
import { ApplicationThemeSchemaNamesEnum } from '../enums';

export class CompiledThemeSchemaFactory {
  public static create(applicationSchema: string): Schema {
    const schema: Schema = new Schema(
      {
        _id: { type: String, default: uuid },
        application: { type: Schema.Types.String, required: true, ref: applicationSchema },
        context: Object,
        data: Object,
        pages: [{ type: Schema.Types.String, required: true, ref: ApplicationThemeSchemaNamesEnum.ApplicationPage }],
        routing: Object,
      },
      {
        minimize: false,
        timestamps: { },
        toJSON: { virtuals: true },
      },
    );

    schema.virtual('id').get(function (): VirtualType {
      return this._id;
    });

    return schema;
  }
}
