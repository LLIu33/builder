import { Schema, VirtualType } from 'mongoose';
import { v4 as uuid } from 'uuid';

export class ApplicationPageSchemaFactory {
  public static create(applicationSchema: string): Schema {
    const schema: Schema = new Schema(
      {
        _id: { type: String, default: uuid },
        application: { type: String, required: true, ref: applicationSchema },
        context: Object,
        data: Object,
        lastActionId: String,
        master: Object,
        name: String,
        stylesheets: Object,
        template: Object,
        type: String,
        variant: String,
      },
      {
        minimize: false,
        timestamps: { },
        toJSON: { virtuals: true },
      },
    );
    schema.virtual('id').get(function (): VirtualType {
      return this._id;
    });

    return schema;
  }

}
