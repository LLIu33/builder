import { Injectable, Logger } from '@nestjs/common';
import { ThemeModel, ThemeService } from '../../themes';
import { ApplicationInterface } from '../interfaces';
import { ApplicationThemeModel } from '../interfaces/entities';
import { TemplateInstallMessagesProducer } from '../producers';
import { ApplicationThemeService } from './application-theme.service';
import { ThemeInstallerService } from './theme-installer.service';

@Injectable()
export class InstantThemeInstallerService {
  constructor(
    private readonly themeInstaller: ThemeInstallerService,
    private readonly appThemeService: ApplicationThemeService,
    private readonly themeService: ThemeService,
    private readonly templateInstallMessagesProducer: TemplateInstallMessagesProducer,
    private readonly logger: Logger,
  ) { }

  public async install(
    application: ApplicationInterface,
    templateTheme: ThemeModel,
  ): Promise<ThemeModel> {
    this.logger.log(`Install template ${templateTheme.id} for application ${application.id}`);
    const installedTheme: ThemeModel = await this.themeInstaller.install(application, templateTheme);
    const appTheme: ApplicationThemeModel =
      await this.appThemeService.activateThemeForApplication(application, installedTheme);
    await this.templateInstallMessagesProducer.sendTempateInstalled(application, appTheme, installedTheme);

    return this.themeService.findById(installedTheme.id);
  }
}
