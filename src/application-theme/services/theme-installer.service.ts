import { Injectable } from '@nestjs/common';
import { PebShop, snapshotToSourceConverter } from '@pe/builder-core';
import { ThemeModel, ThemeService, ThemeType } from '../../themes';
import { ApplicationInterface } from '../interfaces';
import { ApplicationThemeService } from './application-theme.service';


@Injectable()
export class ThemeInstallerService {
  constructor(
    private readonly themeService: ThemeService,
    private readonly applicationThemeService: ApplicationThemeService,
  ) { }

  public async install(
    application: ApplicationInterface,
    templateTheme: ThemeModel,
  ): Promise<ThemeModel> {
    const templateContent: PebShop = snapshotToSourceConverter(templateTheme.source.snapshot as any);
    const templateData: any = templateTheme.toJSON();
    delete templateData._id;
    const theme: ThemeModel = await this.themeService.createFromSource(
      { ...templateData },
      templateContent,
      ThemeType.Application,
    );
    await this.applicationThemeService.create(theme, application);

    return theme;
  }
}
