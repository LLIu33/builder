import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PebShop, snapshotToSourceConverter } from '@pe/builder-core';
import { Model } from 'mongoose';
import * as uuid from 'uuid';
import { ThemeVersionModel } from '../../themes';
import { ApplicationThemeSchemaNamesEnum } from '../enums';
import { ApplicationInterface } from '../interfaces';
import { ApplicationPageModel, CompiledThemeModel } from '../interfaces/entities';

@Injectable()
export class CompiledThemeService {
  constructor (
    @InjectModel(ApplicationThemeSchemaNamesEnum.CompiledTheme)
    private readonly compiledThemeModel: Model<CompiledThemeModel>,
    @InjectModel(ApplicationThemeSchemaNamesEnum.ApplicationPage)
    private readonly applicationPageModel: Model<ApplicationPageModel>,
    private readonly logger: Logger,
  ) { }

  public async compileNewVersion(
    application: ApplicationInterface,
    version: ThemeVersionModel,
  ): Promise<CompiledThemeModel> {
    await version.populate({
      path: 'source',
      populate: {
        path: 'snapshot',
      },
    }).execPopulate();
    this.logger.log(`Convert snaphot: ${version.source.snapshot.id} to source `);
    const compiledSource: PebShop = snapshotToSourceConverter(version.source.snapshot as any);

    await this.compiledThemeModel.deleteMany({ application });
    await this.applicationPageModel.deleteMany({ application });

    this.logger.log(`DTO for create compiledTheme:`);
    this.logger.log({
      application,
      context: compiledSource.context,
      data: compiledSource.data,
      routing: compiledSource.routing,
    });
    const compiledTheme: CompiledThemeModel = await this.compiledThemeModel.create({
      _id: uuid.v4(),
      application,
      context: compiledSource.context,
      data: compiledSource.data,
      pages: [],
      routing: compiledSource.routing,
    });

    const pages: ApplicationPageModel[] = [];
    for (const page of compiledSource.pages) {
      const applicationPage: ApplicationPageModel =
        await this.applicationPageModel.create({
          _id: page.id,
          application,
          context: page.context,
          data: page.data,
          master: page.master,
          name: page.name,
          stylesheets: page.stylesheets,
          template: page.template,
          type: page.type,
          variant: page.variant,
        } as any,
      );

      pages.push(applicationPage);
    }

    return this.compiledThemeModel.findOneAndUpdate(
      { _id: compiledTheme.id },
      { $set: { pages } },
      { new: true },
    );
  }

  public async findByApplicationWithPages(
    application: ApplicationInterface,
    pageVariant?: string,
  ): Promise<CompiledThemeModel> {
    return (pageVariant)
      ? this.compiledThemeModel.findOne({ application })
        .populate({
          match: { variant: pageVariant },
          path: 'pages',
        })
      : this.compiledThemeModel.findOne({ application }).populate('pages');
  }
}
