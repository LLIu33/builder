import { Inject, Injectable } from '@nestjs/common';
import { ProvidersEnum } from '../../common/enums';
import { Model } from 'mongoose';
import { ApplicationInterface } from '../interfaces';
import { BusinessModel } from '../../common/interfaces/entities';

@Injectable()
export class ApplicationService {
  constructor(
    @Inject(ProvidersEnum.ApplicationModel) private readonly application: Model<ApplicationInterface>,
  ) { }

  public async findByIdAndBusiness(id: string, business: BusinessModel): Promise<ApplicationInterface>  {
    return this.application.findOne({
      _id: id,
      business: business.id,
    });
  }

  public async findById(id: string): Promise<ApplicationInterface>  {
    return this.application
      .findOne({
        _id: id,
      })
      .populate('business');
  }
}
