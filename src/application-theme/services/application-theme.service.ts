import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { EventDispatcher } from '@pe/nest-kit';
import { Model } from 'mongoose';
import { ThemeModel } from '../../themes';
import { ApplicationThemeEventsEnum, ApplicationThemeSchemaNamesEnum } from '../enums';
import { ApplicationInterface } from '../interfaces';
import { ApplicationThemeModel } from '../interfaces/entities';

@Injectable()
export class ApplicationThemeService {
  constructor(
    @InjectModel(ApplicationThemeSchemaNamesEnum.ApplicationTheme)
    private readonly applicationThemeModel: Model<ApplicationThemeModel>,
    private readonly eventDispatcher: EventDispatcher,
  ) { }

  public async create(theme: ThemeModel, application: ApplicationInterface): Promise<ApplicationThemeModel> {
    return this.applicationThemeModel.create({
      application: application,
      isActive: false,
      isDeployed: false,
      theme: theme,
    });
  }

  public async publish(appTheme: ApplicationThemeModel): Promise<ApplicationThemeModel> {
    await this.applicationThemeModel.updateMany({ application: appTheme.application }, { $set: { isDeployed: false }});

    return  this.applicationThemeModel.findOneAndUpdate(
      {
        application: appTheme.application,
        theme: appTheme.theme,
      },
      {
        $set: { isDeployed: true },
      },
      { new: true },
    );
  }

  public async activateThemeForApplication(
    application: ApplicationInterface,
    theme: ThemeModel,
  ): Promise<ApplicationThemeModel> {
    await this.applicationThemeModel.updateMany(
      { application },
      { isActive: false },
    );

    return this.applicationThemeModel.findOneAndUpdate(
      {
        application,
        theme: theme,
      },
      { isActive: true },
      { new: true },
    );
  }

  public async findByThemeWithApplication(theme: ThemeModel): Promise<ApplicationThemeModel> {
    return this.applicationThemeModel.findOne({ theme }).populate('application');
  }

  public async findByThemeAndApplication(
    theme: ThemeModel,
    application: ApplicationInterface,
  ): Promise<ApplicationThemeModel> {
    return this.applicationThemeModel.findOne({ theme, application });
  }

  public async remove(appTheme: ApplicationThemeModel): Promise<void> {
    await appTheme.remove();
    await this.eventDispatcher.dispatch(ApplicationThemeEventsEnum.ApplicationThemeRemoved, appTheme);
  }

  public async findByApplication(application: ApplicationInterface): Promise<ApplicationThemeModel[]> {
    return this.applicationThemeModel.find({ application }).populate('theme');
  }

  public findActiveByApplication(application: ApplicationInterface): Promise<ApplicationThemeModel> {
    return this.applicationThemeModel.findOne({ application, isActive: true }) as any;
  }
}
