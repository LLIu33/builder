import { Injectable, Logger } from '@nestjs/common';
import { EventDispatcher } from '@pe/nest-kit';
import { ThemeVersionModel } from '../../themes';
import { ThemePublisherEventsEnum } from '../enums';
import { ApplicationInterface } from '../interfaces';
import { ApplicationThemeModel, CompiledThemeModel } from '../interfaces/entities';
import { CompiledThemeMessagesProducer } from '../producers';
import { ApplicationThemeService } from './';
import { CompiledThemeService } from './compiled-theme.service';

@Injectable()
export class ThemePublisherService {
  constructor(
    private readonly applicationThemeService: ApplicationThemeService,
    private readonly compiledThemeService: CompiledThemeService,
    private readonly compiledThemeMessagesProducer: CompiledThemeMessagesProducer,
    private readonly eventDispatcher: EventDispatcher,
    private readonly logger: Logger,
  ) { }

  public async publish(
    application: ApplicationInterface,
    appTheme: ApplicationThemeModel,
    version: ThemeVersionModel,
  ): Promise<void> {
    try {
      await this.applicationThemeService.publish(appTheme);

      this.logger.log(`Compile theme for application: ${application.id} with version ${version.id}`);
      const compiledTheme: CompiledThemeModel = await this.compiledThemeService.compileNewVersion(application, version);
      this.logger.log(`Created new compiled theme: ${compiledTheme.id}`);
      const themeWithPages: CompiledThemeModel = await this.compiledThemeService
        .findByApplicationWithPages(application);

      await this.eventDispatcher.dispatch(
        ThemePublisherEventsEnum.ThemeVersionPublished,
        appTheme,
        version,
      );

      await this.compiledThemeMessagesProducer.sendThemePublished(appTheme, compiledTheme, themeWithPages.pages);
    } catch (e) {
      this.logger.log(`Publish failed with error: ${JSON.stringify(e)}`);
      this.logger.error(e.message, e.trace, `ThemePublisherService.publish`);
      throw e;
    }
  }
}
