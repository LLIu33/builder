import { Inject } from '@nestjs/common';
import { ProvidersEnum } from '../enums';

export const InjectConfig: any = (): any => Inject(ProvidersEnum.ModuleConfig);
