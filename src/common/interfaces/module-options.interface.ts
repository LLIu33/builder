import { ApplicationTypesEnum } from '../enums';
import { Schema } from 'mongoose';

export interface ModuleOptionsInterface {
  applicationSchemaName: string;
  applicationSchema: Schema;
  applicationType: ApplicationTypesEnum;
  compilationQueuePrefix: string;
  jwtSecret: string;
}
