export enum ApplicationTypesEnum {
  Shop = 'shop',
  Pos = 'pos',
  Blog = 'blog',
  Studio = 'studio',
}
