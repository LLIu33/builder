export enum ProvidersEnum {
  ApplicationModel = 'ApplicationModelProvider',
  ModuleConfig = 'BuilderKitConfig',
}
