import { MessagePattern } from '@nestjs/microservices';
import * as dotenv from 'dotenv';

dotenv.config();

export function DynamicQueuePattern(metadata?: any): MethodDecorator {
  if (!process.env.QUEUE_PREFIX) {
    throw new Error(`Environment variable "QUEUE_PREFIX" is not set`);
  }

  metadata.name = `${process.env.QUEUE_PREFIX}.${metadata.name}`;

  return MessagePattern(metadata);
}
