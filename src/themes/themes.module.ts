import { Global, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {
  ThemeSchema,
  ThemeSnapshotSchema,
  ThemeSourceSchema,
  ThemeVersionSchema,
} from './schemas';
import { ActionsController, ThemesController } from './controllers';
import {
  ThemeActionService,
  ThemeCompilerService,
  ThemeService,
  ThemeSnapshotService,
  ThemeSourceService,
  ThemeVersionService,
} from './services';
import { ApplicationThemeRemoverListener, ThemeVersionPublishedListener } from './event-listeners';
import { ThemeSnapshotMessagesProducer } from './producers';
import { ProvidersEnum } from '../common/enums';
import { ThemesSchemaNamesEnum } from './enums';
import { ModuleOptionsInterface } from '../common/interfaces';
import { UpdateSnapshotConsumer } from './consumers';

@Module({
  controllers: [ ],
  exports: [ ],
  providers: [ ],
})
@Global()
export class ThemesModule {
  public static forRoot(config: ModuleOptionsInterface): any {
    return {
      controllers: [
        ThemesController,
        ActionsController,
        UpdateSnapshotConsumer,
      ],
      exports: [
        ThemeService,
        ThemeVersionService,
      ],
      imports: [
        MongooseModule.forFeature([
          {
            name: ThemesSchemaNamesEnum.Theme,
            schema: ThemeSchema,
          },
          {
            name: ThemesSchemaNamesEnum.ThemeSnapshot,
            schema: ThemeSnapshotSchema,
          },
          {
            name: ThemesSchemaNamesEnum.ThemeSource,
            schema: ThemeSourceSchema,
          },
          {
            name: ThemesSchemaNamesEnum.ThemeVersion,
            schema: ThemeVersionSchema,
          },
        ]),
      ],
      module: ThemesModule,
      providers: [
        ApplicationThemeRemoverListener,
        ThemeService,
        ThemeVersionService,
        ThemeActionService,
        ThemeSnapshotService,
        ThemeSourceService,
        ThemeVersionPublishedListener,
        ThemeCompilerService,
        ThemeSnapshotMessagesProducer,
        {
          provide: ProvidersEnum.ModuleConfig,
          useValue: config,
        },
      ],
    };
  }
}



