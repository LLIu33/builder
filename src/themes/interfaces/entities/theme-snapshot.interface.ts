import { PebContextSchema, PebPageShort, PebStylesheet, PebTemplate } from '@pe/builder-core';

export interface ThemeSnapshotInterface {
  hash: string;
  pages: {
    [key: string]: PebPageShort;
  };
  templates: {
    [key: string]: PebTemplate;
  };
  stylesheets: {
    [key: string]: PebStylesheet;
  };
  contextSchemas: {
    [key: string]: PebContextSchema;
  };

  /** @deprecated */
  shop: any;
}
