import { ThemeInterface } from './theme.interface';
import { Document } from 'mongoose';
import { ThemeSourceModel } from './theme-source.model';
import { ThemeVersionModel } from './theme-version.model';

export interface ThemeModel extends ThemeInterface, Document {
  source: ThemeSourceModel;
  versions: ThemeVersionModel[];
  publishedVersion?: ThemeVersionModel;
}
