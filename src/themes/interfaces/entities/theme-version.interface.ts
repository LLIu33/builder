import { ThemeInterface } from './theme.interface';
import { ThemeSourceInterface } from './theme-source.interface';

export interface ThemeVersionInterface {
  description: string;
  isActive: boolean;
  published: boolean;
  theme: ThemeInterface;
  source: ThemeSourceInterface;
  name: string;
  createdAt: Date;
}
