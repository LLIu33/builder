import { PebAction, PebShopThemeSourcePagePreviews } from '@pe/builder-core';
import { ThemeSnapshotInterface } from './theme-snapshot.interface';

export interface ThemeSourceInterface {
  hash: string;
  actions: PebAction[];
  previews: PebShopThemeSourcePagePreviews;
  snapshot: ThemeSnapshotInterface;
}
