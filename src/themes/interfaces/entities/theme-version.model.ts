import { ThemeVersionInterface } from './theme-version.interface';
import { Document } from 'mongoose';
import { ThemeModel } from './theme.model';
import { ThemeSourceModel } from './theme-source.model';

export interface ThemeVersionModel extends ThemeVersionInterface, Document {
  theme: ThemeModel;
  source: ThemeSourceModel;
}
