import { PebAction } from '@pe/builder-core';
import { Document } from 'mongoose';
import { ThemeSnapshotModel } from './theme-snapshot.model';
import { ThemeSourceInterface } from './theme-source.interface';

export interface ThemeSourceModel extends ThemeSourceInterface, Document {
  snapshot: ThemeSnapshotModel;
  getLastActions(limit: number, offset: number): PebAction[];
}
