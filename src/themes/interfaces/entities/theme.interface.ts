import { ThemeType } from '../../enums';
import { ThemeSourceInterface } from './theme-source.interface';
import { ThemeVersionInterface } from './theme-version.interface';

export interface ThemeInterface {
  type: ThemeType;
  isDefault?: boolean;
  name: string;
  picture: string;
  source: ThemeSourceInterface;
  versions: ThemeVersionInterface[];
  publishedVersion?: ThemeVersionInterface;
}
