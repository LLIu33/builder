import { ThemeSnapshotInterface } from './theme-snapshot.interface';
import { Document } from 'mongoose';

export interface ThemeSnapshotModel extends  ThemeSnapshotInterface, Document { }
