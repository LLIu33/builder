import { Controller } from '@nestjs/common';
import { DynamicQueuePattern } from '../decorators/dynamic-queue-pattern';
import { RabbitMessagesEnum } from '../enums';
import { UpdateSnapshotRequestDto } from '../dto/update-snapshot-request.dto';
import { ThemeCompilerService, ThemeService } from '../services';
import { ThemeModel } from '../interfaces/entities';

@Controller()
export class UpdateSnapshotConsumer {
  constructor(
    private readonly themeService: ThemeService,
    private readonly themeCompiler: ThemeCompilerService,
  ) { }

  @DynamicQueuePattern({
    name: RabbitMessagesEnum.UpdateSnapshot,
  })
  public async onUpdateSnapshotRequest(dto: UpdateSnapshotRequestDto): Promise<void> {
    const theme: ThemeModel = await this.themeService.findById(dto.theme.id);
    await this.themeCompiler.compileTheme(theme);
  }
}
