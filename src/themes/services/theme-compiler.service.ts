import { Injectable, Logger } from '@nestjs/common';
import { PebAction, pebCompileActions } from '@pe/builder-core';
import { EventDispatcher } from '@pe/nest-kit';
import { ThemeEventsEnum } from '../enums';
import { ActionsStateHelper } from '../helpers';
import { ThemeModel, ThemeSnapshotInterface } from '../interfaces/entities';
import { ThemeSnapshotService } from './theme-snapshot.service';

@Injectable()
export class ThemeCompilerService {
  constructor(
    private readonly themeSnapshotService: ThemeSnapshotService,
    private readonly eventDispatcher: EventDispatcher,
    private readonly logger: Logger,
  ) { }

  public async compileTheme(theme: ThemeModel): Promise<void> {
    try {
      await theme
      .populate({
        path: 'source',
        populate: {
          path: 'snapshot',
        },
      })
      .execPopulate();

      const state: string = ActionsStateHelper.getStateHash(theme.source.actions);

      if (state === theme.source.snapshot.hash) {
        return;
      }
      this.logger.log(theme.id, `Try to compile snapshot for theme.id = ${theme.id}`);
      const updatedSnapshot: ThemeSnapshotInterface = await this.compileSnapshot(theme.source.actions);
      await this.themeSnapshotService.update(theme.source.snapshot, updatedSnapshot);
      await this.eventDispatcher.dispatch(ThemeEventsEnum.ThemeSnapshotUpdated, theme);
    } catch (e) {
      await this.eventDispatcher.dispatch(ThemeEventsEnum.ThemeSnapshotUpdateFailed, theme, e);
      this.logger.error(e.message, e.trace, `ThemeActionService.addAction`);
      this.logger.log(`Snapshot compiling failed for source with id ${theme.source.id}`);
      throw e;
    }
  }

  public async compileSnapshot(actions: PebAction[]): Promise<ThemeSnapshotInterface> {
    const hash: string =  ActionsStateHelper.getStateHash(actions);

    return {
      ...pebCompileActions(actions),
      hash,
    } as any;
  }
}
