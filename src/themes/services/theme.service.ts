import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import {
  PebAction,
  pebActionHandler,
  pebCloneShopTheme,
  pebCompileActions,
  pebCreateShopInitAction,
  PebPageShort,
  PebShop,
} from '@pe/builder-core';
import { Model } from 'mongoose';
import {
  ThemeInterface,
  ThemeModel,
  ThemeSnapshotInterface,
  ThemeSnapshotModel,
  ThemeSourceModel,
  ThemeVersionModel,
} from '../interfaces/entities';
import { ThemeCreateDto } from '../dto';
import { ThemeType, ThemesSchemaNamesEnum } from '../enums';
import { ThemePageUpdateActionTransformer } from '../transformers';
import { ThemeCompilerService } from './theme-compiler.service';

@Injectable()
export class ThemeService {
  constructor(
    @InjectModel(ThemesSchemaNamesEnum.Theme) private readonly themeModel: Model<ThemeModel>,
    @InjectModel(ThemesSchemaNamesEnum.ThemeSource) private readonly themeSourceModel: Model<ThemeSourceModel>,
    @InjectModel(ThemesSchemaNamesEnum.ThemeSnapshot) private readonly themeSnapshotModel: Model<ThemeSnapshotModel>,
    private readonly themeCompiler: ThemeCompilerService,
  ) { }

  public async create(createDto: ThemeCreateDto): Promise<ThemeModel> {
    return this.createFromSource(
      {
        name: createDto.name,
        versions: [],
      },
      createDto.content,
      null,
    );
  }

  public async createFromSource(
    themeDto: Partial<ThemeInterface>,
    source: PebShop,
    type: ThemeType,
  ): Promise<ThemeModel> {
    const content: PebShop = pebCloneShopTheme(source);
    const initAction: PebAction = pebCreateShopInitAction(content);

    const snapshot: ThemeSnapshotModel = await this.themeSnapshotModel.create({ } as any);
    const newSource: ThemeSourceModel = await this.themeSourceModel.create(
      {
        actions: [ initAction ],
        snapshot,
      } as any,
    );

    const theme: ThemeModel = await this.themeModel.create({
      ...themeDto,
      source: newSource,
      type,
    } as any);
    await this.themeCompiler.compileTheme(theme);

    return this.findById(theme.id);
  }

  public async addVersion(theme: ThemeModel, version: ThemeVersionModel): Promise<ThemeModel> {
    return this.themeModel.findOneAndUpdate(
      { _id: theme.id },
      {
        $addToSet: {
          versions: version.id,
        },
      },
    );
  }

  public async removeVersion(theme: ThemeModel, version: ThemeVersionModel): Promise<ThemeModel> {
    return this.themeModel.findOneAndUpdate(
      {
        _id: theme.id,
      },
      {
        $pull: {
          versions: version.id,
        },
      },
      { new: true },
    );
  }

  public async findById(themeId: string, populateSnapshot: boolean = false): Promise<ThemeModel> {
    const theme: ThemeModel = await this.themeModel.findOne({
      _id: themeId,
    });

    if (populateSnapshot) {
      await theme
        .populate({
          path: 'source',
          populate: {
            path: 'snapshot',
          },
        })
        .execPopulate();
    }

    return theme;
  }

  public async findDefaultTheme(): Promise<ThemeModel> {
    return this.themeModel.findOne({
      isDefault: true,
    });
  }

  public async findAll(): Promise<ThemeModel[]> {
    return this.themeModel.find();
  }

  public async remove(theme: ThemeModel): Promise<void> {
    await this.themeModel.findOneAndRemove({
      _id: theme.id,
    });
  }

  public async setPreview(theme: ThemeModel, url: string): Promise<ThemeModel> {
    return this.themeModel.findOneAndUpdate(
      {
        _id: theme.id,
      },
      {
        $set: {
          picture: url,
        },
      },
      { new: true },
    );
  }

  public async setThemeType(theme: ThemeModel, type: ThemeType): Promise<ThemeModel> {
    return this.themeModel.findByIdAndUpdate(
      {
        _id: theme.id,
      },
      {
        $set: {
          type: type,
        },
      },
      { new: true },
    );
  }

  public async generateSnapshot(theme: ThemeModel): Promise<ThemeSnapshotInterface> {
    await theme
    .populate({
      path: 'source',
      populate: {
        path: 'snapshot',
      },
    })
    .execPopulate();

    return pebCompileActions(theme.source.actions);
  }

  public async setPagePreview(theme: ThemeModel, pageId: string, data: any): Promise<ThemeModel> {
    await theme.populate('source').execPopulate();
    await theme.source.populate('snapshot').execPopulate();

    const updatePageAction: PebAction = ThemePageUpdateActionTransformer.transform(pageId, data);

    await this.themeSourceModel.findOneAndUpdate(
      {
        _id: theme.source.id,
      },
      {
        $set: {
          actions: [...theme.source.actions, updatePageAction],
        },
      },
      { new: true },
    );

    const snapshotInterface: ThemeSnapshotInterface =
      pebActionHandler(theme.source.snapshot.toObject(), updatePageAction);

    await this.themeSnapshotModel.findOneAndUpdate(
      {
        _id: theme.source.snapshot.id,
      },
      {
        $set: {
          ...snapshotInterface,
        },
      },
      { new: true },
    );

    theme = await this.themeModel.findOne({
      _id: theme.id,
    });


    return theme;
  }

  public async findThemeSourceById(id: string): Promise<ThemeSourceModel> {
    return this.themeSourceModel.findOne({
      _id: id,
    });
  }

  public async removeApplicationTheme(theme: ThemeModel): Promise<void> {
    await this.themeModel.deleteOne({
      _id: theme.id,
      type: ThemeType.Application,
    });
  }

  public async getPageSnapshot(theme: ThemeModel, pageId: string): Promise<any> {
    await theme
    .populate({
      path: 'source',
      populate: {
        path: 'snapshot',
      },
    })
    .execPopulate();

    const snapshot: ThemeSnapshotModel = theme.source.snapshot.toJSON();
    const page: PebPageShort = snapshot.pages[pageId];
    if (!page) {
      return false;
    }
    snapshot.pages = { [page.id]: page };

    const stylesheets: { [key: string]: any } = { };
    Object.values(page.stylesheetIds).forEach((styleId: string) => {
      stylesheets[styleId] = snapshot.stylesheets[styleId];
    });
    snapshot.stylesheets = stylesheets;

    snapshot.contextSchemas = (snapshot.contextSchemas[page.contextId])
      ? { [page.contextId]: snapshot.contextSchemas[page.contextId] }
      : { };

    snapshot.templates = (snapshot.templates[page.templateId])
      ? { [page.templateId]: snapshot.templates[page.templateId] }
      : { };

    return snapshot;
  }

  public async setName(theme: ThemeModel, name: string): Promise<ThemeModel> {
    return this.themeModel.findOneAndUpdate(
      {
        _id: theme.id,
      },
      {
        $set: {
          name: name,
        },
      },
      { new: true },
    );
  }
}
