import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { extractPageActionsFromSnapshot, PebAction, pebActionHandler, PebPageShort, PebPageType } from '@pe/builder-core';
import { Mutex } from '@pe/nest-kit';
import { ThemeModel, ThemeSnapshotInterface, ThemeSnapshotModel } from '../interfaces/entities';
import { ThemeSnapshotMessagesProducer } from '../producers';
import { ThemeCompilerService } from './theme-compiler.service';
import { ThemeSnapshotService } from './theme-snapshot.service';
import { ThemeSourceService } from './theme-source.service';

const MUTEX_NAMESPACE: string = 'adding-action';

@Injectable()
export class ThemeActionService {
  constructor(
    private readonly themeSourceService: ThemeSourceService,
    private readonly themeSnapshotService: ThemeSnapshotService,
    private readonly themeCompiler: ThemeCompilerService,
    private readonly themeSnapshotMessagesProducer: ThemeSnapshotMessagesProducer,
    private readonly mutex: Mutex,
    private readonly logger: Logger,
  ) { }

  public async getActions(theme: ThemeModel, limit: number, offset: number): Promise<PebAction[]> {
    await theme.populate('source').execPopulate();

    return theme.source.getLastActions(limit, offset);
  }

  public async addAction(theme: ThemeModel, action: PebAction): Promise<void> {
    try {
      await this.mutex.lock(MUTEX_NAMESPACE, theme.id, async () => {
        this.logger.log(`Try to add action with id: ${action.id}`);
        // tslint:disable
        console.time('theme-populate');
        await theme
          .populate({
            path: 'source',
            populate: {
              path: 'snapshot',
            },
          })
          .execPopulate();
        console.timeEnd('theme-populate');

        console.time('pebActionHandler');
        const updateSnapshotData: ThemeSnapshotInterface = pebActionHandler(theme.source.snapshot.toObject(), action);
        console.timeEnd('pebActionHandler');
        if (updateSnapshotData) {
          console.time('addAction');
          await this.themeSourceService.addAction(theme.source, action);
          console.timeEnd('addAction');
          console.time('updateSnapshot');
          await this.themeSnapshotService.update(theme.source.snapshot, updateSnapshotData);
          console.timeEnd('updateSnapshot');
        }
        // tslint:enable
      });
    } catch (e) {
      this.logger.error(e.message, e.trace, `ThemeActionService.addAction`);
      throw new BadRequestException(`Wrong request: for action with the id ${action.id}`);
    }
  }

  public async deleteAction(theme: ThemeModel, actionId: string): Promise<void> {
    await theme
      .populate({
        path: 'source',
        populate: {
          path: 'snapshot',
        },
      })
      .execPopulate();

    await this.themeSourceService.deleteActionById(theme.source, actionId);
    await this.themeSnapshotMessagesProducer.sendUpdateSnapshotMessage(theme);
  }

  public async replaceInitPageActions(
    theme: ThemeModel,
    actions: PebAction[],
  ): Promise<ThemeSnapshotModel> {
    const actionsToReplace: { [key: string]: PebAction } = { };
    actions.forEach( (action: PebAction) => {
      const pageActions: PebAction[] = extractPageActionsFromSnapshot(
        theme.source.actions,
        theme.source.snapshot as any,
        action.targetPageId,
      );

      if (pageActions[0] && pageActions[0].id) {
        actionsToReplace[pageActions[0].id] = action;
      }
    });
    await this.mutex.lock(MUTEX_NAMESPACE, theme.id, async () => {
      await this.themeSourceService.replaceActions(theme.source, actionsToReplace);
      await this.themeCompiler.compileTheme(theme);
    });

    return this.themeSnapshotService.findById(theme.source.snapshot.id);
  }
}
