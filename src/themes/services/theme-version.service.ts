import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ThemeVersionCreateDto } from '../dto';
import { ThemesSchemaNamesEnum } from '../enums';
import { ThemeModel, ThemeSnapshotModel, ThemeSourceModel, ThemeVersionModel } from '../interfaces/entities';
import { ThemeSnapshotService } from './theme-snapshot.service';
import { ThemeSourceService } from './theme-source.service';
import { ThemeService } from './theme.service';

@Injectable()
export class ThemeVersionService {
  constructor(
    @InjectModel(ThemesSchemaNamesEnum.ThemeVersion) private readonly themeVersionModel: Model<ThemeVersionModel>,
    private readonly themeSnapshotService: ThemeSnapshotService,
    private readonly themeSourceService: ThemeSourceService,
    private readonly themeService: ThemeService,
    private readonly logger: Logger,
  ) { }

  public async create(
    theme: ThemeModel,
    input: ThemeVersionCreateDto,
  ): Promise<ThemeVersionModel> {
    await theme.populate({
      path: 'source',
      populate: {
        path: 'snapshot',
      },
    }).execPopulate();

    try {
      const versionSnapshot: ThemeSnapshotModel = await this.themeSnapshotService.create({
        contextSchemas: theme.source.snapshot.contextSchemas,
        pages: theme.source.snapshot.pages,
        shop: theme.source.snapshot.shop,
        stylesheets: theme.source.snapshot.stylesheets,
        templates: theme.source.snapshot.templates,
      } as any);

      const versionSource: ThemeSourceModel = await this.themeSourceService.create({
        actions: theme.source.actions,
        snapshot: versionSnapshot,
      } as any);

      const themeVersion: ThemeVersionModel = await this.themeVersionModel.create({
        description: input.description,
        name: input.name,
        published: false,
        result: null,
        source: versionSource,
        theme: theme,
      } as any);

      await this.setActiveVersion(themeVersion);
      await this.themeService.addVersion(theme, themeVersion);

      return this.themeVersionModel.findOne({ _id: themeVersion.id });
    } catch (e) {
      this.logger.error(e.message, e.trace, `ThemeVersionService.create`);
      throw e;
    }
  }

  public async publish(version: ThemeVersionModel): Promise<ThemeVersionModel> {
    await this.themeVersionModel.updateMany({ theme: version.theme }, { $set: { published: false }});

    return this.themeVersionModel.findOneAndUpdate({ _id: version.id }, { $set: { published: true }});
  }

  public async getActiveVersion(theme: ThemeModel): Promise<ThemeVersionModel> {
    return this.themeVersionModel.findOne({ theme: theme, isActive: true });
  }

  public async setActiveVersion(version: ThemeVersionModel): Promise<ThemeVersionModel> {
    await this.themeVersionModel.updateMany({ theme: version.theme }, { $set: { isActive: false }});

    return this.themeVersionModel.findOneAndUpdate({ _id: version.id }, { $set: { isActive: true }});
  }

  public async remove(version: ThemeVersionModel, theme: ThemeModel): Promise<void> {
    await version.populate({
      path: 'source',
      populate: {
        path: 'snapshot',
      },
    }).execPopulate();

    await this.themeVersionModel.findOneAndRemove({ _id: version.id });

    await this.themeService.removeVersion(theme, version);
    await this.themeSourceService.removeById(version.source.id);
    await this.themeSnapshotService.removeById(version.source.snapshot.id);
  }

  public async findById(id: string): Promise<ThemeVersionModel> {
    return this.themeVersionModel.findOne({ _id: id });
  }

  public async getPublishedVersion(theme: ThemeModel): Promise<ThemeVersionModel> {
    return this.themeVersionModel.findOne({ _id: { $in: theme.versions }, published: true});
  }

  public async restore(version: ThemeVersionModel, theme: ThemeModel): Promise<void> {
    await this.themeSourceService.copy(version.source, theme.source);
    await this.themeSnapshotService.copy(version.source.snapshot, theme.source.snapshot);
  }
}
