import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PebAction } from '@pe/builder-core';
import { Model } from 'mongoose';
import * as uuid from 'uuid';
import { ThemesSchemaNamesEnum } from '../enums';
import { ThemeSourceInterface, ThemeSourceModel } from '../interfaces/entities';

@Injectable()
export class ThemeSourceService {
  constructor (
    @InjectModel(ThemesSchemaNamesEnum.ThemeSource) private readonly themeSourceModel: Model<ThemeSourceModel>,
  ) { }

  public async create(source: ThemeSourceInterface): Promise<ThemeSourceModel>  {
    return this.themeSourceModel.create({
      _id: uuid.v4(),
      actions: source.actions,
      snapshot: source.snapshot,
    } as any);
  }

  public async addAction(source: ThemeSourceModel, action: PebAction): Promise<ThemeSourceModel> {
    return this.themeSourceModel.findOneAndUpdate(
      {
        _id: source.id,
      },
      {
        $set: {
          actions: [...source.actions, action],
        },
      },
      {
        new: true,
      },
    );
  }

  public async replaceActions(
    source: ThemeSourceModel,
    actionsToReplace: { [key: string]: PebAction },
  ): Promise<ThemeSourceModel> {
    const newActions: PebAction[] = source.actions.map( (action: PebAction) => {
      return actionsToReplace[action.id] ? actionsToReplace[action.id] : action;
    });

    return this.themeSourceModel.findOneAndUpdate(
      {
        _id: source.id,
      },
      {
        $set: {
          actions: newActions,
        },
      },
      {
        new: true,
      },
    );
  }

  public async setPreviews(source: ThemeSourceModel, previews: any): Promise<ThemeSourceModel> {
    return this.themeSourceModel.findOneAndUpdate(
      {
        _id: source.id,
      },
      {
        $set: {
          previews: previews,
        },
      },
      {
        new: true,
      },
    );
  }

  public async deleteActionById(source: ThemeSourceModel, actionId: string): Promise<ThemeSourceModel> {
    return this.themeSourceModel.findOneAndUpdate(
      {
        _id: source.id,
      },
      {
        $set: {
          actions: source.actions.filter((a: PebAction) => a.id !== actionId),
        },
      },
      {
        new: true,
      },
    );

  }

  public async removeById(id: string): Promise<void> {
    await this.themeSourceModel.findOneAndDelete({ _id: id });
  }

  public async copy(origin: ThemeSourceModel, target: ThemeSourceModel): Promise<ThemeSourceModel> {
    target.actions = origin.actions;
    target.previews = origin.previews;

    return target.save();
  }
}
