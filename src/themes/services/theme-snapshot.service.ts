import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as uuid from 'uuid';
import { ThemesSchemaNamesEnum } from '../enums';
import { ThemeSnapshotInterface, ThemeSnapshotModel } from '../interfaces/entities';

@Injectable()
export class ThemeSnapshotService {
  constructor(
    @InjectModel(ThemesSchemaNamesEnum.ThemeSnapshot) private readonly themeSnapshotModel: Model<ThemeSnapshotModel>,
    private readonly logger: Logger,
  ) { }

  public async findById(snapshotId: string): Promise<ThemeSnapshotModel> {
    return this.themeSnapshotModel.findOne({
      _id: snapshotId,
    });
  }

  public async create(snapshot: ThemeSnapshotInterface): Promise<ThemeSnapshotModel> {
    return this.themeSnapshotModel.create({
      _id: uuid.v4(),
      contextSchemas: snapshot.contextSchemas,
      pages: snapshot.pages,
      shop: snapshot.shop,
      stylesheets: snapshot.stylesheets,
      templates: snapshot.templates,
    } as any);
  }

  public async update(snapshot: ThemeSnapshotModel, updateData: ThemeSnapshotInterface): Promise<ThemeSnapshotModel> {
    return this.themeSnapshotModel.findOneAndUpdate(
      {
        _id: snapshot.id,
      },
      {
        $set: {
          ...updateData,
        },
      },
    );
  }

  public async removeById(id: any): Promise<void> {
    await this.themeSnapshotModel.findOneAndDelete({ _id: id });
  }

  public async copy(origin: ThemeSnapshotModel, target: ThemeSnapshotModel): Promise<ThemeSnapshotModel> {
    target.contextSchemas = origin.contextSchemas;
    target.hash = origin.hash;
    target.pages = origin.pages;
    target.shop = origin.shop;
    target.stylesheets = origin.stylesheets;
    target.templates = origin.templates;

    return target.save();
  }
}
