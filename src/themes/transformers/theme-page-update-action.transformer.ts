import { PebAction, PebEffectTarget, pebGenerateId, PebPageEffect } from '@pe/builder-core';

export class ThemePageUpdateActionTransformer {
  public static transform(pageId: string, payload: any): PebAction {
    return {
      affectedPageIds: [],
      createdAt: new Date(),
      effects: [
        {
          payload,
          target: `${PebEffectTarget.Pages}:${pageId}`,
          type: PebPageEffect.Update,
        },
      ],
      id: pebGenerateId('action'),
      targetPageId: pageId,
    };
  }
}
