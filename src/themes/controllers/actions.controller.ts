import { Body, Controller, Delete, Get, NotFoundException, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { PebAction, PebPageShort } from '@pe/builder-core';
import { AbstractController, JwtAuthGuard, ParamModel, Roles, RolesEnum } from '@pe/nest-kit';
import { ThemesSchemaNamesEnum } from '../enums';
import { ThemeModel, ThemeSnapshotModel } from '../interfaces/entities';
import { ThemeActionService } from '../services';

@Controller()
@UseGuards(JwtAuthGuard)
@Roles(RolesEnum.merchant)
export class ActionsController extends AbstractController {
  constructor(
    private readonly themeActionService: ThemeActionService,
  ) {
    super();
  }

  @Get('theme/:themeId/actions')
  public async getActions(
    @ParamModel(':themeId', ThemesSchemaNamesEnum.Theme) theme: ThemeModel,
    @Query('limit') limit: string,
    @Query('offset') offset: string,
  ): Promise<any> {
    const limitValue: number = parseInt(limit, 10);
    const offsetValue: number = parseInt(offset, 10);

    return this.themeActionService.getActions(theme, limitValue, offsetValue);
  }

  @Post('theme/:themeId/action')
  public async addAction(
    @ParamModel(':themeId', ThemesSchemaNamesEnum.Theme) theme: ThemeModel,
    @Body() action: PebAction,
  ): Promise<any> {
    await this.themeActionService.addAction(theme, action);
  }

  @Delete('theme/:themeId/action/:actionId')
  public async deleteLastAction(
    @ParamModel(':themeId', ThemesSchemaNamesEnum.Theme) theme: ThemeModel,
    @Param('actionId') actionId: string,
  ): Promise<any> {
    await this.themeActionService.deleteAction(theme, actionId);
  }

  @Put('/theme/:themeId/actions/apply')
  public async replaceInitPageActions(
    @ParamModel(':themeId', ThemesSchemaNamesEnum.Theme) theme: ThemeModel,
    @Body() actions: PebAction[],
  ): Promise<ThemeSnapshotModel> {
    await theme.populate({
      path: 'source',
      populate: {
        path: 'snapshot',
      },
    })
    .execPopulate();

    return this.themeActionService.replaceInitPageActions(theme, actions);
  }
}
