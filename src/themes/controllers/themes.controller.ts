import { Body, Controller, Get, NotFoundException, Param, Patch, Post, Put, UseGuards } from '@nestjs/common';
import { JwtAuthGuard, ParamModel, Roles, RolesEnum } from '@pe/nest-kit';
import { ImagePreviewDto, ThemeCreateDto, ThemeUpdateDto } from '../dto';
import { ThemesSchemaNamesEnum } from '../enums';
import { ThemeModel, ThemeSnapshotModel, ThemeSourceModel } from '../interfaces/entities';
import { ThemeService, ThemeSourceService } from '../services';

@Controller()
@UseGuards(JwtAuthGuard)
@Roles(RolesEnum.merchant)
export class ThemesController {
  constructor(
    private readonly themeService: ThemeService,
    private readonly themeSourceService: ThemeSourceService,
  ) { }

  @Get('themes')
  public async getList(): Promise<ThemeModel[]> {
    return this.themeService.findAll();
  }

  @Post('theme')
  public async create(
    @Body() dto: ThemeCreateDto,
  ): Promise<ThemeModel> {
    return this.themeService.create(dto);
  }

  @Get('theme/:themeId')
  public async getById(
    @ParamModel(':themeId', ThemesSchemaNamesEnum.Theme) theme: ThemeModel,
  ): Promise<any> {
    await theme.populate('source').execPopulate();
    const result: any = theme.toJSON();
    delete result.source.actions;

    return result;
  }

  @Patch('theme/:themeId/name')
  public async updateTheme(
    @ParamModel(':themeId', ThemesSchemaNamesEnum.Theme) theme: ThemeModel,
    @Body() input: ThemeUpdateDto,
  ): Promise<ThemeModel> {
    return this.themeService.setName(theme, input.name);
  }

  @Put('theme/:themeId/image-preview')
  public async postUpdateImagePreview(
    @ParamModel(':themeId', ThemesSchemaNamesEnum.Theme) theme: ThemeModel,
    @Body() input: ImagePreviewDto,
  ): Promise<ThemeModel> {
    return this.themeService.setPreview(theme, input.imagePreview);
  }

  @Put('theme/:themeId/page/:pageId/image-preview')
  public async postUpdatePageImagePreview(
    @ParamModel(':themeId', ThemesSchemaNamesEnum.Theme) theme: ThemeModel,
    @Param('pageId') pageId: string,
    @Body() input: any,
  ): Promise<ThemeModel> {

    if (theme) {
      await theme.populate({
        path: 'source',
        populate: {
          path: 'snapshot',
        },
      })
      .execPopulate();
    }

    return this.themeService.setPagePreview(theme, pageId, input.data);
  }

  @Patch('/theme/:themeId/source/:sourceId/previews')
  public async updatePreviews(
    @Param('sourceId') sourceId: string,
    @Body() previews: any,
  ): Promise<any> {
    const source: ThemeSourceModel = await this.themeService.findThemeSourceById(sourceId);
    source.previews = previews;
    await this.themeSourceService.setPreviews(source, previews);

    return source.previews;
  }

  @Get('theme/:themeId/snapshot')
  public async getActualSnapshot(
    @ParamModel(':themeId', ThemesSchemaNamesEnum.Theme) theme: ThemeModel,
  ): Promise<ThemeSnapshotModel> {
    await theme.populate({
      path: 'source',
      populate: {
        path: 'snapshot',
      },
    })
    .execPopulate();

    return theme.source.snapshot;
  }

  @Get('theme/:themeId/pages')
  public async getPages(
    @ParamModel(':themeId', ThemesSchemaNamesEnum.Theme) theme: ThemeModel,
  ): Promise<any> {
    await theme.populate({
      path: 'source',
      populate: {
        path: 'snapshot',
      },
    })
    .execPopulate();

    return theme.source.snapshot.pages;
  }

  @Get('theme/:themeId/snapshot/:pageId')
  public async getPartialSnapshot(
    @ParamModel(':themeId', ThemesSchemaNamesEnum.Theme) theme: ThemeModel,
    @Param('pageId') pageId: string,
  ): Promise<ThemeSnapshotModel> {
    await theme.populate({
      path: 'source',
      populate: {
        path: 'snapshot',
      },
    })
    .execPopulate();

    return this.themeService.getPageSnapshot(theme, pageId);
  }
}
