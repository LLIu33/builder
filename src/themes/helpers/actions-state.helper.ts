import { hashString, PebAction } from '@pe/builder-core';

export class ActionsStateHelper {
  public static getStateHash(actions: PebAction[]): string {
    return hashString(JSON.stringify(actions));
  }
}
