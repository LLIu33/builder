export enum ThemeEventsEnum {
  ThemeSnapshotUpdateFailed = 'theme.snapshot.update-failed',
  ThemeSnapshotUpdated = 'theme.snapshot.updated',
}
