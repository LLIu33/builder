export enum ThemesSchemaNamesEnum {
  Theme = 'Theme',
  ThemeVersion = 'ThemeVersion',
  ThemeSnapshot = 'ThemeSnapshot',
  ThemeSource = 'ThemeSource',
}
