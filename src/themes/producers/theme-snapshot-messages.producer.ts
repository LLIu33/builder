import { Injectable } from '@nestjs/common';
import { RabbitMqClient } from '@pe/nest-kit';
import { ThemeModel } from '../interfaces/entities';
import { RabbitMessagesEnum } from '../enums';
import { ModuleOptionsInterface } from '../../common/interfaces';
import { InjectConfig } from '../../common/decorators';

@Injectable()
export class ThemeSnapshotMessagesProducer {
  constructor(
    private readonly rabbitClient: RabbitMqClient,
    @InjectConfig() private readonly config: ModuleOptionsInterface,
  ) { }

  public async sendUpdateSnapshotMessage(theme: ThemeModel): Promise<void> {

    const queueName: string = `${this.config.compilationQueuePrefix}.${RabbitMessagesEnum.UpdateSnapshot}`;
    await this.rabbitClient.send(
      {
        channel: queueName,
        exchange: 'async_events',
      },
      {
        name: queueName,
        payload: {
          theme: {
            id: theme.id,
          },
        },
      },
    );
  }
}
