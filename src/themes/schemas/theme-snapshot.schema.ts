import { Schema, VirtualType } from 'mongoose';
import { v4 as uuid } from 'uuid';

export const ThemeSnapshotSchema: Schema = new Schema(
  {
    _id: { type: String, default: uuid },
    contextSchemas: Object,
    hash: String,
    pages: Object,
    shop: Object,
    stylesheets: Object,
    templates: Object,
  },
  {
    minimize: false,
    timestamps: { },
    toJSON: { virtuals: true },
  },
);

ThemeSnapshotSchema.virtual('id').get(function (): VirtualType {
  return this._id;
});
