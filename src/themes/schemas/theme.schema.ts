import { Schema, VirtualType } from 'mongoose';
import { v4 as uuid } from 'uuid';
import { ThemesSchemaNamesEnum } from '../enums';

export const ThemeSchema: Schema = new Schema(
  {
    _id: { type: String, default: uuid },
    isDefault: Boolean,
    name: String,
    picture: String,
    publishedVersion: { type: Schema.Types.String, ref: ThemesSchemaNamesEnum.ThemeVersion },
    source: { type: Schema.Types.String, ref: ThemesSchemaNamesEnum.ThemeSource },
    type: String,
    versions: {
      default: [],
      type: [{ type: Schema.Types.String, ref: ThemesSchemaNamesEnum.ThemeVersion }],
    },
  },
  {
    minimize: false,
    timestamps: { },
    toJSON: { virtuals: true },
  },
);

ThemeSchema.virtual('id').get(function (): VirtualType {
  return this._id;
});
