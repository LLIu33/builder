import { PebAction } from '@pe/builder-core';
import { Schema, VirtualType } from 'mongoose';
import { v4 as uuid } from 'uuid';
import { ThemesSchemaNamesEnum } from '../enums';

export const ThemeSourceSchema: Schema = new Schema(
  {
    _id: { type: String, default: uuid },
    actions: [{ type: Schema.Types.Mixed }],
    hash: String,
    previews: Schema.Types.Mixed,
    snapshot: { type: Schema.Types.String, ref: ThemesSchemaNamesEnum.ThemeSnapshot },
  },
  {
    minimize: false,
    timestamps: { },
    toJSON: { virtuals: true },
  },
);

ThemeSourceSchema.virtual('id').get(function (): VirtualType {
  return this._id;
});

ThemeSourceSchema.methods.getLastActions = function (limit: number, offset: number): PebAction[] {
  if (!limit || isNaN(limit)) {
    return this.actions;
  }
  offset = (!offset || isNaN(offset)) ? 0 : offset;
  const startIndex: number = Math.max(this.actions.length - (limit + offset), 0);
  const endIndex: number = startIndex + limit;

  return this.actions.slice(startIndex, endIndex);
};
