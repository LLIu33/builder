import { Schema, VirtualType } from 'mongoose';
import { v4 as uuid } from 'uuid';
import { ThemesSchemaNamesEnum } from '../enums';

export const ThemeVersionSchema: Schema = new Schema(
  {
    _id: { type: String, default: uuid },
    isActive: Boolean,
    name: String,
    published: Boolean,
    result: Object,
    source: { type: Schema.Types.String, ref: ThemesSchemaNamesEnum.ThemeSource },
    theme: { type: Schema.Types.String, ref: ThemesSchemaNamesEnum.Theme },
  },
  {
    minimize: false,
    timestamps: { },
    toJSON: { virtuals: true },
  },
);

ThemeVersionSchema.virtual('id').get(function (): VirtualType {
  return this._id;
});
