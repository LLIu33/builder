import { IsDefined, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { ThemeReferenceDto } from './theme-reference.dto';

export class UpdateSnapshotRequestDto {
  @ValidateNested()
  @IsDefined()
  @Type(() => ThemeReferenceDto)
  public theme: ThemeReferenceDto;
}
