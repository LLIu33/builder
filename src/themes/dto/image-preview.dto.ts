import { IsNotEmpty, IsString } from 'class-validator';

export class ImagePreviewDto {
  @IsString()
  @IsNotEmpty()
  public imagePreview: string;
}
