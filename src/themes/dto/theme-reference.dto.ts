import { IsNotEmpty, IsString } from 'class-validator';

export class ThemeReferenceDto {
  @IsString()
  @IsNotEmpty()
  public id: string;
}
