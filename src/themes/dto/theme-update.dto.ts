import { IsObject, IsString } from 'class-validator';

export class ThemeUpdateDto {
  @IsString()
  public name: string;
}
