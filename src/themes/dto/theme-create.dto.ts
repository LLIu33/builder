import { IsObject, IsString } from 'class-validator';
import { PebShop } from '@pe/builder-core';

export class ThemeCreateDto {
  @IsString()
  public name: string;

  @IsObject()
  public content: PebShop;
}
