import { Injectable } from '@nestjs/common';
import { EventListener } from '@pe/nest-kit';
import { ApplicationThemeEventsEnum } from '../../application-theme/enums';
import { ThemeService } from '../services';
import { ApplicationThemeModel } from '../../application-theme/interfaces/entities';

@Injectable()
export class ApplicationThemeRemoverListener {

  constructor (
    private readonly themeService: ThemeService,
  ) { }

  @EventListener(ApplicationThemeEventsEnum.ApplicationThemeRemoved)
  public async onAppThemeRemoved(appTheme: ApplicationThemeModel): Promise<void> {
    if (!appTheme.populated('theme')) {
      await appTheme.populate('theme').execPopulate();
    }

    await this.themeService.removeApplicationTheme(appTheme.theme);
  }
}
