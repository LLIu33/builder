import { Injectable } from '@nestjs/common';
import { EventListener } from '@pe/nest-kit';
import { ThemePublisherEventsEnum } from '../../application-theme/enums';
import { ThemeVersionModel } from '../interfaces/entities';
import { ThemeVersionService } from '../services';

@Injectable()
export class ThemeVersionPublishedListener {

  constructor (
    private readonly themeVersionService: ThemeVersionService,
  ) { }

  @EventListener(ThemePublisherEventsEnum.ThemeVersionPublished)
  public async onThemeVersionPublished(_: any, version: ThemeVersionModel): Promise<void> {
    await this.themeVersionService.publish(version);
  }
}
