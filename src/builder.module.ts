import { BadRequestException, Global, Module } from '@nestjs/common';
import { ErrorHandlersEnum, ErrorsHandlerModule, EventDispatcherModule, MutexModule } from '@pe/nest-kit';
import { ApplicationThemeModule } from './application-theme';
import { ModuleOptionsInterface } from './common/interfaces';
import { TemplateThemeModule } from './template-theme';
import { ThemesModule } from './themes';
import { WebsocketsModule } from './websockets';

@Module({
  controllers: [],
  exports: [],
  imports: [],
  providers: [],
})
@Global()
export class BuilderModule {
  public static forRoot(config: ModuleOptionsInterface): any {
    return {
      controllers: [],
      exports: [],
      imports: [
        MutexModule,
        ThemesModule.forRoot(config),
        ApplicationThemeModule.forRoot(config),
        TemplateThemeModule,
        EventDispatcherModule,
        WebsocketsModule.forRoot(config),
        ErrorsHandlerModule.forRoot([{
          exceptions: [BadRequestException],
          name: ErrorHandlersEnum.dtoValidation,
        }]),
      ],
      module: BuilderModule,
      providers: [],
    };
  }
}

