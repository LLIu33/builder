import { Injectable } from '@nestjs/common';
import { ThemeModel } from '../../themes';

@Injectable()
export class ThemeWebsocketPublisher {
  public async sendSnapshotUpdatedMessage(client: WebSocket, theme: ThemeModel): Promise<void> {

  }
}
