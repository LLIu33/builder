import { SubscribeMessage, WebSocketGateway } from '@nestjs/websockets';
import { Inject } from '@nestjs/common';
import { verify as jwtVerify } from 'jsonwebtoken';
import { v4 as uuid } from 'uuid';
import * as WebSocket from 'ws';
import { ThemeWebsocketMessagesEnum } from '../enums';
import { ConnectPayloadDto } from '../dto';
import { FailedConnectResponseInterface, SuccessConnectResponseInterface } from '../interfaces';
import { ProvidersEnum } from '../../common/enums';
import { ModuleOptionsInterface } from '../../common/interfaces';
import { ThemeModel } from '../../themes';

@WebSocketGateway()
export class ThemesGateway {
  private clients: { [clientId: string]: WebSocket } = { };
  private themesSubscriptions: { [themeId: string]: string[] } = { };
  private jwtSecret: string;

  public constructor(
    @Inject(ProvidersEnum.ModuleConfig) private readonly config: ModuleOptionsInterface,
  ) {
    setInterval(
      () => {
        this.clearDisconnected();
      },
      10 * 1000,
    );
    this.jwtSecret = this.config.jwtSecret;
  }

  @SubscribeMessage(ThemeWebsocketMessagesEnum.Connect)
  public async onConnectEvent(
    client: WebSocket,
    payload: ConnectPayloadDto,
  ): Promise<SuccessConnectResponseInterface | FailedConnectResponseInterface> {
    const token: string = payload.token;
    if (!this.verify(token)) {
      return {
        name: ThemeWebsocketMessagesEnum.Connect,
        result: false,
      };
    }

    const id: string = uuid();
    this.saveClient(payload, id, client);

    return {
      id: id,
      name: ThemeWebsocketMessagesEnum.Connect,
      result: true,
    };

  }

  public async sendThemeSnapshotUpdated(theme: ThemeModel): Promise<void> {
    for (const clientId of this.findClientsForTheme(theme.id)) {
      await this.sendMessage(
        clientId,
        {
          name: ThemeWebsocketMessagesEnum.SnapshotUpdated,
          result: true,
          snapshot: theme.source.snapshot,
          theme: {
            id: theme.id,
          },
        },
      );
    }
  }

  public async sendThemeSnapshotUpdateFailed(theme: ThemeModel, errorMessage: string): Promise<void> {
    for (const clientId of this.findClientsForTheme(theme.id)) {
      await this.sendMessage(
        clientId,
        {
          errorMessage,
          name: ThemeWebsocketMessagesEnum.SnapshotUpdateFailed,
          result: false,
          theme: {
            id: theme.id,
          },
        },
      );
    }
  }

  private clearDisconnected(): void {
    for (const clientIds in this.themesSubscriptions) {
      if (!this.themesSubscriptions.hasOwnProperty(clientIds)) {
        continue;
      }
      for (const clientId of this.themesSubscriptions[clientIds]) {
        const client: WebSocket = this.findClient(clientId);
        switch (client.readyState) {
          case client.CLOSING:
          case client.CLOSED:
            this.removeClient(clientId);
            break;
        }
      }
    }
  }

  private removeClient(clientId: string): void {
    delete this.clients[clientId];

    for (const themeClients in this.themesSubscriptions) {
      if (this.themesSubscriptions.hasOwnProperty(themeClients)) {
        continue;
      }
      const index: number = this.themesSubscriptions[themeClients].findIndex((element: string) => element === clientId);
      this.themesSubscriptions[themeClients].splice(index, 1);
    }

  }

  private verify(token: string): boolean {
    try {
      jwtVerify(token, this.jwtSecret);
    } catch (e) {
      return false;
    }

    return true;
  }

  private saveClient(payload: ConnectPayloadDto, clientId: string, client: WebSocket): void {
    if (!this.themesSubscriptions[payload.themeId]) {
      this.themesSubscriptions[payload.themeId] = [];
    }

    this.themesSubscriptions[payload.themeId].push(clientId);
    this.clients[clientId] = client;
  }

  private findClient(clientId: string): WebSocket {
    return this.clients[clientId];
  }

  private findClientsForTheme(themeId: string): any[] {
    return this.themesSubscriptions[themeId] || [];
  }

  private async sendMessage(clientId: string, message: any): Promise<void> {
    try {
      const client: WebSocket = this.findClient(clientId);
      switch (client.readyState) {
        case client.OPEN:
          client.send(
            JSON.stringify(message),
          );
          break;
        case client.CLOSING:
        case client.CLOSED:
          this.removeClient(clientId);
      }
    } catch (e) {
      this.removeClient(clientId);
    }
  }

}
