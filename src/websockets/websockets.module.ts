import { Global, Module } from '@nestjs/common';
import { ThemesGateway } from './gateway';
import { ProvidersEnum } from '../common/enums';
import { ModuleOptionsInterface } from '../common/interfaces';

@Module({
  controllers: [],
  exports: [],
  providers: [],
})
@Global()
export class WebsocketsModule {
  public static forRoot(config: ModuleOptionsInterface): any {
    return {
      controllers: [],
      exports: [],
      imports: [],
      module: WebsocketsModule,
      providers: [
        ThemesGateway,
        {
          provide: ProvidersEnum.ModuleConfig,
          useValue: config,
        },
      ],
    };
  }
}



