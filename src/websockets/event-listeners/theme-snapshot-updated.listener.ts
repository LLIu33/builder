import { Injectable } from '@nestjs/common';
import { EventListener } from '@pe/nest-kit';
import { ThemeEventsEnum, ThemeModel } from '../../themes';
import { ThemesGateway } from '../gateway';

@Injectable()
export class ThemeSnapshotUpdatedListener {
  constructor(
    private readonly gateway: ThemesGateway,
  ) { }

  @EventListener(ThemeEventsEnum.ThemeSnapshotUpdated)
  public async onSnapshotUpdated(theme: ThemeModel): Promise<void>  {
    await this.gateway.sendThemeSnapshotUpdated(theme);
  }

  @EventListener(ThemeEventsEnum.ThemeSnapshotUpdateFailed)
  public async onSnapshotUpdateFailed(theme: ThemeModel, e: Error): Promise<void>  {
    await this.gateway.sendThemeSnapshotUpdateFailed(theme, e.message);
  }
}
