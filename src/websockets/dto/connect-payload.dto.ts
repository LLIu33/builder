import { IsNotEmpty, IsString } from 'class-validator';

export class ConnectPayloadDto {
  @IsString()
  @IsNotEmpty()
  public token: string;

  @IsString()
  @IsNotEmpty()
  public themeId: string;
}
