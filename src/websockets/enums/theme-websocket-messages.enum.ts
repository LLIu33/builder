export enum ThemeWebsocketMessagesEnum {
  SnapshotUpdateFailed = 'snapshot.update-failed',
  Connect = 'connect',
  SnapshotUpdated = 'snapshot.updated',
}
