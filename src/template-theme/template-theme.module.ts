import { Global, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {
  TemplateThemeGroupSchema,
  TemplateThemeItemSchema,
} from './schemas';
import { TemplateSchemaNamesEnum } from './enums';
import { ThemesModule } from '../themes';
import { TemplateThemeService } from './services';
import { TemplateThemeController } from './controllers';

@Module({
  controllers: [
    TemplateThemeController,
  ],
  exports: [ ],
  imports: [
    MongooseModule.forFeature([
      {
        name: TemplateSchemaNamesEnum.TemplateThemeItem,
        schema: TemplateThemeItemSchema,
      },
      {
        name: TemplateSchemaNamesEnum.TemplateThemeGroup,
        schema: TemplateThemeGroupSchema,
      },
    ]),
    ThemesModule,
  ],
  providers: [
    TemplateThemeService,
  ],
})
@Global()
export class TemplateThemeModule { }



