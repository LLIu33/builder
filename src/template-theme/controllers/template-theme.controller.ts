import { Body, Controller, Get, MethodNotAllowedException, Post, UseGuards } from '@nestjs/common';
import { JwtAuthGuard, ParamModel, Roles, RolesEnum  } from '@pe/nest-kit';
import { TemplateThemeGroupModel } from '../interfaces/entities';
import { TemplateThemeDto } from '../dto';
import { TemplateThemeService } from '../services';
import { ThemeModel, ThemeService, ThemeType, ThemesSchemaNamesEnum } from '../../themes';



@Controller()
@UseGuards(JwtAuthGuard)
@Roles(RolesEnum.merchant)
export class TemplateThemeController {
  constructor(
    private readonly templateThemeService: TemplateThemeService,
    private readonly themeService: ThemeService,
  ) { }

  @Get('/templates')
  public async getAllTemplateThemes(): Promise<TemplateThemeGroupModel[]> {
    return this.templateThemeService.findAllOrderByOrder();
  }

  @Post('/:themeId/template')
  public async markThemeAsTemplate(
    @Body() input: TemplateThemeDto,
    @ParamModel(':themeId', ThemesSchemaNamesEnum.Theme, true) theme: ThemeModel,
  ): Promise<TemplateThemeGroupModel> {
    if (theme.type === ThemeType.Application) {
      throw new MethodNotAllowedException(
        'You can\'t set this theme as a template. It already has a application type.',
      );
    }

    await this.themeService.setThemeType(theme, ThemeType.Template);

    return this.templateThemeService.create(theme, input);
  }
}
