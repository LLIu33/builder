import { Document } from 'mongoose';
import { ThemeModel } from '../../../themes/interfaces';
import { TemplateThemeItemInterface } from './template-theme-item.interface';


export interface TemplateThemeItemModel extends TemplateThemeItemInterface, Document {
  themes?: ThemeModel[];
}
