import { Document } from 'mongoose';
import { TemplateThemeGroupInterface } from './template-theme-group.interface';
import { TemplateThemeItemModel } from './template-theme-item.model';

export interface TemplateThemeGroupModel extends TemplateThemeGroupInterface, Document {
  items?: TemplateThemeItemModel[];
}
