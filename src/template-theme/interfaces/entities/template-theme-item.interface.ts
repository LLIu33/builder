import { ThemeInterface } from '../../../themes/interfaces';
import { ThemeTemplateTypeEnum } from '../../enums';

export interface TemplateThemeItemInterface {
  code: string;
  type: ThemeTemplateTypeEnum;
  themes?: ThemeInterface[];
}
