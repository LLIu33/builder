import { TemplateThemeItemInterface } from './template-theme-item.interface';

export interface TemplateThemeGroupInterface {
  code: string;
  icon?: string;
  order: number;
  items?: TemplateThemeItemInterface[];
}
