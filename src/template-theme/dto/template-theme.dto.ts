import { IsEnum, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { ThemeTemplateTypeEnum } from '../enums';

export class TemplateThemeDto {
  @IsNotEmpty()
  @IsString()
  public codeGroup: string;

  @IsNotEmpty()
  @IsNumber()
  public order: number;

  @IsNotEmpty()
  @IsString()
  public codeItem: string;

  @IsEnum(ThemeTemplateTypeEnum)
  public type: ThemeTemplateTypeEnum;
}
