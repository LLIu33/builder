export enum ThemeTemplateTypeEnum {
  Product = 'product',
  Color = 'color',
  Season = 'season',
  Style = 'style',
  Media = 'media',
}
