export enum TemplateSchemaNamesEnum {
  TemplateThemeGroup = 'TemplateThemeGroup',
  TemplateThemeItem = 'TemplateThemeItem',
}
