import { Schema, VirtualType } from 'mongoose';
import { v4 as uuid } from 'uuid';
import { ThemesSchemaNamesEnum } from '../../themes';

export const TemplateThemeItemSchema: Schema = new Schema(
  {
    _id: { type: String, default: uuid },
    code: String,
    themes: [{ type: String, required: true, ref: ThemesSchemaNamesEnum.Theme }],
    type: String,
  },
  {
    timestamps: { },
    toJSON: { virtuals: true },
  },
);

TemplateThemeItemSchema.virtual('id').get(function (): VirtualType {
  return this._id;
});
