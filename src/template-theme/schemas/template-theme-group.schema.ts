import { Schema, VirtualType } from 'mongoose';
import { v4 as uuid } from 'uuid';
import { TemplateSchemaNamesEnum } from '../enums';

export const TemplateThemeGroupSchema: Schema = new Schema(
  {
    _id: { type: String, default: uuid },
    code: String,
    icon: String,
    items: [{ type: String, ref: TemplateSchemaNamesEnum.TemplateThemeItem }],
    order: Number,
  },
  {
    timestamps: { },
    toJSON: { virtuals: true },
  },
);

TemplateThemeGroupSchema.virtual('id').get(function (): VirtualType {
  return this._id;
});
