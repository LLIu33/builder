import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { TemplateThemeDto } from '../dto';
import { TemplateThemeGroupModel, TemplateThemeItemModel } from '../interfaces/entities';
import { ThemeModel } from '../../themes';
import { TemplateSchemaNamesEnum } from '../enums';

@Injectable()
export class TemplateThemeService {
  constructor(
    @InjectModel(TemplateSchemaNamesEnum.TemplateThemeItem) 
    private readonly templateThemeItemModel: Model<TemplateThemeItemModel>,
    @InjectModel(TemplateSchemaNamesEnum.TemplateThemeGroup) 
    private readonly templateThemeGroupModel: Model<TemplateThemeGroupModel>,
  ) { }

  public async create(theme: ThemeModel, input: TemplateThemeDto): Promise<TemplateThemeGroupModel> {
    const templateItem: TemplateThemeItemModel = await this.findOrCreateTemplateItem(input);
    await this.addThemeToItem(templateItem, theme);

    const templateGroup: TemplateThemeGroupModel = await this.findOrCreateTemplateGroup(input);
    await this.addItemToGroup(templateGroup, templateItem);

    return this.templateThemeGroupModel
      .findOne({ _id: templateGroup.id })
      .populate({
        path: 'items',
        populate: {
          path: 'themes',
        },
      },
    );
  }

  public async findAll(): Promise<TemplateThemeGroupModel[]> {
    return this.templateThemeGroupModel
      .find()
      .populate(
        {
          path: 'items',
          populate: {
            path: 'themes',
          },
        },
      );
  }

  public async findAllOrderByOrder(): Promise<TemplateThemeGroupModel[]> {
    return this.templateThemeGroupModel
      .find()
      .sort({ order: 1 })
      .populate(
        {
          path: 'items',
          populate: {
            path: 'themes',
          },
        },
      );
  }

  private async findOrCreateTemplateItem(input: TemplateThemeDto): Promise<TemplateThemeItemModel>  {
    const templateItem: TemplateThemeItemModel = await this.templateThemeItemModel.findOne({
      code: input.codeItem,
    });

    if (templateItem) {
      return templateItem;
    }

    return this.templateThemeItemModel.create({
      code: input.codeItem,
      type: input.type,
    });
  }

  private async addThemeToItem(item: TemplateThemeItemModel, theme: ThemeModel): Promise<TemplateThemeItemModel> {
    return this.templateThemeItemModel.findOneAndUpdate(
      {
        _id: item.id,
      },
      {
        $addToSet: {
          themes: theme.id,
        },
      },
      { new: true },
    );
  }

  private async findOrCreateTemplateGroup(input: TemplateThemeDto): Promise<TemplateThemeGroupModel> {
    const templateGroup: TemplateThemeGroupModel =
      await this.templateThemeGroupModel.findOne({ code: input.codeGroup });
    if (templateGroup) {
      return templateGroup;
    }

    return this.templateThemeGroupModel.create({
      code: input.codeGroup,
      order: input.order,
    });
  }

  private async addItemToGroup(
    group: TemplateThemeGroupModel,
    item: TemplateThemeItemModel,
  ): Promise<TemplateThemeGroupModel> {
    return this.templateThemeGroupModel.findOneAndUpdate(
      {
        _id: group.id,
      },
      {
        $addToSet: {
          items: item.id,
        },
      },
    );
  }
}
